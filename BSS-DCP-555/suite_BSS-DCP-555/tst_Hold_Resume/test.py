"""
#$Id: On Hold/Call Hold(a=sendonly)

Name:
     On Hold/Call Hold(a=sendonly)

Test Case Coverage:
     This script will cover below test cases:
     1. Sec1_6 : On Hold/Call Hold(a=sendonly)
     2. Sec1_6 : On Hold/Call Hold(a=recvonly)
     3. Sec1_7 : UnHold/Resume Button



Author:
     Awadhesh Kumar (awadhesh.kumar@harman.com)

Purpose:
     To test DUT should able to Hold and Resume an active call.

Description:
      From Line 1 DUT dials the number for Line 2, and receives the call at line 2.
      From Line 1 Hold the call, and then resume after some time.

Test bed requirement:
      One DCP-555 device

Test Steps:
    1. From Line 1, Launch the VoIP dialer, and dial Line 2 number
    2. Navigate to line 2, Answer the call
    3. Navigate to Line 1 and Hold the call then resume after some time

Verify:
      - DUT successfully establishes the call session.
      - SIP Flow:
        Line 1                   Server
          | ----- INVITE --------> | # With Hold SDP having attribute a=sendonly
          | <----- 200 ----------- | # With Held SDP, having attribute a=recvonly
          | ------- ACK ---------> |
          ========= No RTP =========
          | ----- INVITE --------> | # With Resume SDP having attribute a=sendrecv
          | <----- 200 ----------- | # 200OK may have a=sendrecv (Optional)
          | ------- ACK ---------> |

End of Header
#################################################################
"""

source(findFile("scripts", "/device/PageObject/ApplicationUnderTest.py"))
source(findFile("scripts", "suite_init.py"))
source(findFile("scripts", "Utils/tshark.py"))


def main():
    device = BssDcp555()
    tshark = Tshark("device1")
    login_page = device.open_web(url="device1")
    base_page = login_page.login(password="device_password")
    voip_settings = base_page.get_setting_page("VoIP")

    # Navigate to line 1
    voip_settings.navigate_to_line("line1")
    voip_settings.wait("....", wait_seconds=2)
    voip_settings.register_line("device1", "line1")
    voip_settings.wait("....", wait_seconds=2)

    # Navigate to Line 2
    voip_settings.navigate_to_line(2)
    user2, server = voip_settings.register_line("dev1", "Line2")

    # Start wireshark capture
    cap_file = tshark.start_tcpdump(file_name)

    # Making call from Line 1 to Line 2
    voip_settings.navigate_to_line("line1")
    voip_settings.launch_dialer()
    voip_settings.wait("Waiting for VoIP Dialer Page", wait_seconds=2)
    voip_settings.dial(user2)
    voip_settings.press_call_button()
    assert voip_settings.verify_call_state() is True
    voip_settings.close_dialer()

    voip_settings.wait("Waiting to close dialer", wait_seconds=2)
    voip_settings.navigate_to_line("Line2")
    voip_settings.launch_dialer()
    voip_settings.wait("Waiting for VoIP Dialer Page", wait_seconds=2)
    assert voip_settings.verify_accept_button_enabled()
    voip_settings.accept_call()
    assert voip_settings.verify_call_state() is True
    voip_settings.close_dialer()
    voip_settings.wait("Waiting to close dialer", wait_seconds=2)

    # Holding the call
    voip_settings.navigate_to_line("line1")
    voip_settings.launch_dialer()
    voip_settings.wait("Waiting for VoIP Dialer Page", wait_seconds=2)
    voip_settings.press_hold_button()
    assert voip_settings.verify_resume_button_is_enabled() is True
    voip_settings.wait("Call get Hold", wait_seconds=5)

    # Resuming the call
    voip_settings.press_resume_button()
    assert voip_settings.verify_hold_button_is_enabled() is True
    voip_settings.wait("For call get resumed", wait_seconds=2)

    # Ending call from line 2
    voip_settings.press_end_call_button()
    voip_settings.wait("To get call disconnected", wait_seconds=2)
    assert voip_settings.verify_call_state() is False
    # Closing Dialer
    voip_settings.close_dialer()
    
    device.close_web()

    # Start Wireshark Analysis
    tshark.stop_tcpdump()
    capt_file = tshark.download_captured_file(cap_file)
    cap = tshark.get_packet_filter(capt_file, "device1", server)
    num = tshark.verify_hold_flow(cap, "device1", server)
    tshark.verify_resume_flow(cap, "device1", server, ignore_pkt=num)
    
