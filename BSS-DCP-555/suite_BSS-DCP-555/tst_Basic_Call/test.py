"""
#$Id: Outgoing dialing number

Name:
     Outgoing dialing number

Test Case Coverage:
     This script will cover below test cases:
     1. Sec1_1 : Outgoing dialing number
     2. Sec1_2 : Call Dial Button
     3. Sec1_3 : Call End Button
     4. Sec1_4 : Accept Button


Author:
     Awadhesh Kumar (awadhesh.kumar@harman.com)

Purpose:
     To test DUT successfully establish a call and gracefully ends the call

Description:
      From Line 1 DUT dials the number for Line 2, and receives the call at line 2.
      After successfully establishing the call, DUT ends the call from Line 2.

Test bed requirement:
      One DCP-555 device

Test Steps:
    1. From Line 1, Launch the VoIP dialer, and dial Line 2 number
    2. Navigate to line 2, Answer the call
    3. End the call from line 2

Verify:
      - DUT successfully establishes the call session.
      - SIP Flow:
        Line 1                   Server
          | ----- INVITE --------> |
          | <----- 401 ----------- |
          | ------- ACK ---------> |
          | ----- INVITE --------> |
          | <----- 180 ----------- |
          | <----- 200 ----------- |
          | ------- ACK ---------> |
          ========= RTP ============
          | <----- BYE ----------- |
          | ------- 200 ---------> |

End of Header
#################################################################
"""

source(findFile("scripts", "/device/PageObject/ApplicationUnderTest.py"))
source(findFile("scripts", "suite_init.py"))
source(findFile("scripts", "Utils/tshark.py"))


def main():

    tshark = Tshark("device1")
    device = BssDcp555()
    login_page = device.open_web(url="device1")
    base_page = login_page.login(password="device_password")
    voip_settings = base_page.get_setting_page("VoIP")
    
    # Navigate to line 1
    voip_settings.navigate_to_line("line1")
    device.wait("....", wait_seconds=2)
    voip_settings.register_line("device1", "line1")
    device.wait("....", wait_seconds=2)

    # Navigate to Line 2
    voip_settings.navigate_to_line(2)
    user2, server = voip_settings.register_line("dev1", "Line2")
    test.verify(voip_settings.verify_registration_status() is True, "Checking line registration status")
    device.wait("....", wait_seconds=2)
    
    # Start wireshark capture
    cap_file = tshark.start_tcpdump(file_name) 
    
    voip_settings.navigate_to_line("line1")
    device.wait("....", wait_seconds=2)
    voip_settings.launch_dialer()
    voip_settings.wait("Waiting for VoIP Dialer Page", wait_seconds=2)
    voip_settings.dial(user2)
    voip_settings.press_call_button()
#     assert voip_settings.verify_call_state() is True
    test.verify(voip_settings.verify_call_state() == True, "verifying if the call")
    voip_settings.close_dialer()
    
    voip_settings.wait("Waiting to close dialer", wait_seconds=2)
    voip_settings.navigate_to_line("Line2")
    voip_settings.launch_dialer()
    voip_settings.wait("Waiting for VoIP Dialer Page", wait_seconds=2)
    test.verify(voip_settings.verify_accept_button_enabled()is True, "Verifying Accept Button is Enabled")
    voip_settings.accept_call()

    voip_settings.wait("Just waiting", wait_seconds=10)
    test.verify(voip_settings.verify_call_state() == True, "verifying Call is active")
    # Ending call from line 2
    voip_settings.press_end_call_button()
    voip_settings.wait("Waiting for call get disconnected", wait_seconds=2)
    test.verify(voip_settings.verify_call_state() is False, "verifying Call is inactive")
#     assert voip_settings.verify_call_state() is False
    # Closing Dialer
    voip_settings.close_dialer()
    device.close_web()
    
    # Wireshark analysis
    tshark.stop_tcpdump()
    capt_file = tshark.download_captured_file(cap_file)
    cap = tshark.get_packet_filter(capt_file, "device1", server)
    
    test.verify(tshark.verify_outbound_call_flow(cap, "device1", server)is True, "Verifying Outbound Call Flow")
    test.verify(tshark.verify_inbound_call_flow(cap, "device1", server) is True, "Verifying Inbound call flow")
    
