"""
#$Id: Registration of SIP EP with UDP

Name:
     Registration of SIP EP with UDP

Test Case Coverage:
     This script will cover below test cases:
     1. Sec2_5 : Registration of SIP EP with UDP
     2. Sec2_5 : Response with Authenticate Header
     3. Sec2_5 : REGISTER with Authorization Header
     4. Sec2_5 : Authentication User name
     5. Sec2_5 : Successful Registration
     6. Sec2_1 : Account Name
     7. Sec2_2 : Display Name
     8. Sec2_2 : Appearance of Display Name in SIP messages
     9: Sec2_3 : User Name/Number
     10: Sec2_3 : Appearance of User in SIP messages
     11: Sec2_4 : Domain Name with IP
     12: Sec2_4 : Domain Name with Domain Name


Author:
     Awadhesh Kumar (awadhesh.kumar@harman.com)

Purpose:
     To test DUT successfully accept configuration parameters and able to register to proxy
     with proxy/domain address as IP or FQDN

Description:
      In DUT VoIP Line settings, set the value of Username, Authentication name, Password, Display Name 
      and Domain Name. 
      Click on Reconfigure

Test bed requirement:
      One DCP-555 device

Test Steps:
    1. In VoIP Line 1 Settings
    2. Set values for Username, Authentication  Name, Password, Display Name and Domain Name
    3. Click on Reconfigure

Verify:
      - DUT successfully registered to the device.
      - SIP Flow:
        Line 1                   Server
          | ----- REGISTER ------> |
          | <----- 401 ----------- |
          | ----- REGISTER ------> |
          | <----- 200 ----------- |

End of Header
#################################################################
"""

source(findFile("scripts", "/device/PageObject/ApplicationUnderTest.py"))
source(findFile("scripts", "suite_init.py"))
source(findFile("scripts", "Utils/tshark.py"))


def main():
    device = BssDcp555()
    tshark = Tshark("device1")
    login_page = device.open_web(url="device1")
    base_page = login_page.login(password="device_password")
    voip_settings = base_page.get_setting_page("VoIP")

    # Start tcpdump on device
    cap_file = tshark.start_tcpdump(file_name)
    
    # Navigate to line 1
    voip_settings.navigate_to_line("line1")
    voip_settings.wait("....", wait_seconds=2)

    user, server = voip_settings.register_line("dev1", "Line1")

    voip_settings.wait("....", wait_seconds=2)
    
    # Start Wireshark Analysis
    tshark.stop_tcpdump()
    capt_file = tshark.download_captured_file(cap_file)
    cap = tshark.get_packet_filter(capt_file, "device1", server)
    tshark.verify_register_flow(cap, "device1", server, user)
    
    device.close_web()

