"""
#$Id: Incoming Call to DUT Reject  Button

Name:
     Incoming Call to DUT Reject  Button

Test Case Coverage:
     This script will cover below test cases:
     1. Sec1_5 : Incoming Call to DUT Reject  Button

Author:
     Awadhesh Kumar (awadhesh.kumar@harman.com)

Purpose:
     To test DUT successfully Rejects the incoming call

Description:
      From Line 1 DUT dials the number for Line 2, and reject the call from
      line 2

Test bed requirement:
      One DCP-555 device

Test Steps:
    1. From Line 1, Launch the VoIP dialer, and dial Line 2 number
    2. Navigate to line 2, Reject the call

Verify:
      - DUT successfully Rejects the incoming call.
      - SIP Flow:
        Line 1                   Server
          | ----- INVITE --------> |
          | <----- 401 ----------- |
          | ------- ACK ---------> |
          | ----- INVITE --------> |
          | <----- 180 ----------- |
          | <----- 486 ----------- |
          | ------- ACK ---------> |

End of Header
#################################################################
"""

source(findFile("scripts", "/device/PageObject/ApplicationUnderTest.py"))
source(findFile("scripts", "suite_init.py"))
source(findFile("scripts", "Utils/tshark.py"))


def main():
    device = BssDcp555()
    tshark = Tshark("device1")
    login_page = device.open_web(url="device1")
    base_page = login_page.login(password="device_password")
    voip_settings = base_page.get_setting_page("VoIP")

    # Navigate to line 1
    voip_settings.navigate_to_line("line1")
    voip_settings.wait("....", wait_seconds=2)
    voip_settings.register_line("device1", "Line1")
    voip_settings.wait("....", wait_seconds=2)

    # Navigate to Line 2
    voip_settings.navigate_to_line(2)
    user2, server = voip_settings.register_line("dev1", "Line2")

    # Start wireshark capture
    cap_file = tshark.start_tcpdump(file_name)

    # Making call from Line 1 to Line 2
    voip_settings.navigate_to_line("1")
    voip_settings.launch_dialer()
    voip_settings.wait("Waiting for VoIP Dialer Page", wait_seconds=2)
    voip_settings.dial(user2)
    voip_settings.press_call_button()
    assert voip_settings.verify_call_state() is True
    voip_settings.close_dialer()

    # Navigating to Line 2
    voip_settings.wait("Waiting to close dialer", wait_seconds=2)
    voip_settings.navigate_to_line("Line2")
    voip_settings.launch_dialer()
    voip_settings.wait("Waiting for VoIP Dialer Page", wait_seconds=2)
    # Rejecting the incoming call
    assert voip_settings.verify_reject_button_enabled()
    voip_settings.reject_call()

    voip_settings.wait("Waiting to close dialer", wait_seconds=2)
    voip_settings.close_dialer()

    # Start Wireshark Analysis
    tshark.stop_tcpdump()
    capt_file = tshark.download_captured_file(cap_file)
    cap = tshark.get_packet_filter(capt_file, "device1", server)
    tshark.verify_outbound_reject_call_flow(cap, "device1", server)
    tshark.verify_inbound_reject_call_flow(cap, "device1", server)
    
    device.close_web()

