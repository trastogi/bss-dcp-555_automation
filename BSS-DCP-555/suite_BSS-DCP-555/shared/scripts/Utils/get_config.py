import ConfigParser
import os


class GetConfig:
    """
    This class extract the configuration data form configuration file
    """
 
#     def __init__(self):
#         self.parser = ConfigParser.ConfigParser()
#         self.parser.read(os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), 'config')),'config.ini'))
          
    def get_data(self, section_name, data):
        parser = ConfigParser.ConfigParser()
        parser.read(os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'shared/scripts/config')),'config.ini'))
        value = parser.get(section_name, data)
        return value 

