import os
import re
import socket
import sys

import paramiko

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


global config, log

class SshUtil:
    """
    Class to connect test device using SSH
    """

    def __init__(self, device):
        self.ssh_output = None
        self.ssh_error = None
        self.client = paramiko.SSHClient()
        self.host = config.get_data("device", device)
        self.username = config.get_data('ssh_config', "username")
        self.password = config.get_data("ssh_config", "username")
        self.timeout = float(config.get_data("ssh_config", "timeout"))
        self.commands = ""
        self.port = config.get_data("ssh_config", "port")
        self.upload_remote_file_path = config.get_data("ssh_config", "upload_remote_file_path")
        self.upload_local_file_path = config.get_data("ssh_config", "upload_local_file_path")
        self.download_remote_file_path = config.get_data("ssh_config", "download_remote_file_path")
        self.download_local_file_path = config.get_data("ssh_config", "download_local_file_path")

    def connect(self):
        """
        Connecting and login into the device
        :return:
        """
        test.log("Establishing connection to the device: {}".format(self.host))
        try:
            # Parsing an instance of the AutoAddPolicy to set_missing_host_key_policy() changes it to allow any host.
            self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            # setting up the credentials to login into the device
            self.client.connect(hostname=self.host, port=self.port, username=self.username, password=self.password,
                                timeout=self.timeout, allow_agent=False, look_for_keys=False)
            test.log("Connected to the device")
        except paramiko.AuthenticationException:
            test.log("Authentication Failed, check credentials")
            result_flag = False
        except paramiko.SSHException as sshException:
            test.log("Could not establish SSH Connection {}".format(sshException))
            result_flag = False
        except socket.timeout as e:
            test.log("Connection Time Out")
            result_flag = False
        except Exception as exception:
            test.log("Error in connecting to device")
            test.log("Error: {}".format(exception))
            result_flag = False
            self.client.close()
        else:
            result_flag = True

        return result_flag

    def run_command(self, command, timeout=10):
        """
        Execute a command on remote device. Return a tuple containing an integer status, and two strings.
        First string containing stdout and second containing the stderr, from the command given.
        :param timeout: timeout
        :param command: Command to execute. e.g. ls
        :return: command output
        """
        self.ssh_output = None

        try:
            if self.connect():
                test.log("Executing command: {}".format(command))
                if re.search(".*sudo.*", command):
                    stdin, stdout, stderr = self.client.exec_command(command, timeout=timeout, get_pty=True)
                    stdin.write(self.password + '\n')
                    stdin.flush()
                else:
                    stdin, stdout, stderr = self.client.exec_command(command, timeout=timeout)

                self.ssh_output = stdout.read()
                self.ssh_error = stderr.read()

                if self.ssh_error:
                    test.log("Problem occurred while running command: {}.".format(command))
                    test.log("The error is {}.".format(self.ssh_error))
                    return self.ssh_error
                else:
                    test.log("Command execution completed successfully. {}".format(command))
                    return self.ssh_output
        except socket.timeout:
            test.log("Command timed out. {}".format(command))
            self.client.close()
        except paramiko.SSHException:
            test.log("Failed to execute the command. {}".format(command))
            self.client.close()

    def download_file(self, download_remote_file_path, download_local_file_path):
        """
        Function to download file from server
        :param download_remote_file_path: file path on the remote device
        :param download_local_file_path: file path on the local machine
        :return: True or False
        """
        result_flag = True

        try:
            if self.connect():
                ftp_client = self.client.open_sftp()
                ftp_client.get(download_remote_file_path, download_local_file_path)
                test.log("Downloading the file: {}".format(download_remote_file_path))
                ftp_client.close()
                self.client.close()
                test.log("Download Success")
            else:
                test.log("Could not establish SSH connection")
                result_flag = False
        except Exception as e:
            test.log("Unable to download file. {}".format(download_local_file_path))
            test.log("Error: {}".format(e))
            result_flag = False
            self.client.open_sftp().close()
            self.client.close()

        return result_flag

    def upload_file(self, upload_local_file_path, upload_remote_file_path):
        """
        Function to upload file on remote device
        :param upload_local_file_path: file path on the local machine
        :param upload_remote_file_path: file path on the remote device
        :return: True or False
        """
        result_flag = True

        try:
            if self.connect():
                ftp_client = self.client.open_sftp()
                ftp_client.put(upload_local_file_path, upload_remote_file_path)
                test.log("Uploading the file: {}".format(upload_local_file_path))
                ftp_client.close()
                self.client.close()
                test.log("Upload Success")
            else:
                test.log("Could not establish SSH connection")
                result_flag = False
        except Exception as e:
            test.log("Unable to download file. {}".format(upload_remote_file_path))
            test.log("Error: {}".format(e))
            result_flag = False
            self.client.open_sftp().close()
            self.client.close()

        return result_flag
