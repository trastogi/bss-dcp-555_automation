import logging
import os
import re
import time

import pyshark

source(findFile("scripts", "Utils/ssh_util.py"))


global config, log

class Tshark:

    """
    Class to run tcpdump on device, save the dumpfile and download it and check network packets in downloaded file
    for correct messages
    """

    def __init__(self, device):
        self.ssh_client = SshUtil(device)
        self.interface = config.get_data("tshark", "interface")
        self.download_path = None
        self.remote_path = None
        self.current_time = None

    def get_tcpdump_start_time(self):
        """
        Function to get the time of start of tcpdump
        :return: string contains time
        """
        self.current_time = time.strftime("%d-%b-%Y-%H%M%S", time.localtime(time.time()))
        return self.current_time

    def start_tcpdump(self, fname):
        """
        Function to start tcpdump on device
        :return:
        """
        try:
            self.stop_tcpdump()
            test.log("Closed previously running instances of tcpdump")
        except IndexError:
            test.log("tcpdump was not previously running")

        name = os.path.splitext(os.path.basename(fname))
        start_time = self.get_tcpdump_start_time()
        file_name = "{}-{}.pcap".format(name[0], start_time)
        try:
            output = self.ssh_client.run_command("sudo tcpdump -i {} -w {}".format(self.interface, file_name), timeout=2)
            if output != "tcpdump: command not found":
                test.log("tcpdump started")
            else:
                raise AssertionError("tcpdump is not installed")
#             pid = self.get_tcpdump_pid()
#             if pid:
#                 test.log("tcpdump started")
#             else:
#                 test.log("Unable to start tcpdump")
        except Exception as e:
            test.log(str(e))

        return file_name

    def get_tcpdump_pid(self):
        """
        Function to get tcpdump pid on device, by executing command 'ps -ef' on device
        :return: returns the pid
        """
        output = self.ssh_client.run_command("ps -ef | grep tcpdump")
        pattern1 = re.compile(' *([0-9]+) +.sudo.*tcpdump')
        pattern2 = re.compile(' *([0-9]+) +.*tcpdump')
        pid = pattern1.findall(output)
        if pid:
            return pid
        else:
            pid = pattern2.findall(output)
            return pid

    def stop_tcpdump(self):
        """
        Function to stop tcpdump, by executing command "kill -9 <pid>'
        :return:
        """
        pids = self.get_tcpdump_pid()
        for pid in pids:
            self.ssh_client.run_command("sudo kill -9 {}".format(pid))
            test.log("Killed tcpdump PID: {}".format(pid))

    # def check_tcpdump_is_installed(self):
    #     """
    #     Function to check device has tcpdump installed
    #     :return: True or False
    #     """
    #     output = self.ssh_client.run_command("sudo tcpdump -h")
    #     pattern = re.compile('command not found')
    #     if pattern.finditer(output):
    #         self.test.log("Install tcpdump to device")
    #         return False
    #     else:
    #         return True

    def download_captured_file(self, cap_file):
        """
        Function to download wireshark/tcpdump captured file from device to local machine
        :param cap_file: file path or file name
        :return:
        """
        self.download_path = os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), '..',
                                                                       'shared/scripts/Logs/Wireshark_logs')), '{}'.format(cap_file))
        self.remote_path = self.ssh_client.download_remote_file_path+cap_file
        test.log(str(self.remote_path))
        if self.ssh_client.download_file(self.remote_path, self.download_path):
            test.log("Removing capture file from device")
            self.ssh_client.run_command("rm -rf {}".format(self.remote_path))
            return self.download_path
        else:
            test.log("Download failed")
            self.ssh_client.run_command("rm -rf {}".format(self.remote_path))
            test.log("Removing capture file from device")
#             raise AssertionError("Failed to download file")

    def get_packet_filter(self, pcap_file, device, server, protocol='sip'):
        """
        Function to filter wireshark capture file for related protocol
        :param pcap_file: capture file
        :param device: device ip
        :param server: server ip
        :param protocol: communication protocol, e.g. SIP
        :return: filtered capture file
        """
        protocol = protocol.lower()
        device_ip = config.get_data("device", device)
        server_ip = server
        display_filter = "{} &&  (ip.addr=={} && ip.addr=={})".format(protocol, device_ip, server_ip)

        cap = pyshark.FileCapture(pcap_file, display_filter=display_filter)
        return cap
    
    def verify_transport_protocol(self, cap, device, server, transport="UDP", method_name="register"):
        """
        Functiom to verify transport protocol in SIP messages
        """
        device_ip = config.get_data("device", device)
        server_ip = server
        method_name = method_name.upper()
        transport = transport.upper()
        if transport == 'UDPONLY':
            transport = 'UDP'
        elif transport == 'TCP':
            transport = 'TCP'
        elif transport == 'AUTO':
            transport = 'UDP'
    
        num = 0
        found = False
        for pkt in cap:
            if pkt.highest_layer.find("SIP") != -1:
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is False:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find(method_name) != -1:
                                if cap[num]['sip'].via_transport == transport:
                                    found = True
                                    test.log("SIP messages tramsport mode is: {}".format(cap[num]['sip'].via_transport))
                                    break
            num += 1
        if found is False:
            return found
        else:
            return found

    def verify_register_flow(self, cap, device, server, user):
        """

        :param user:
        :param cap:
        :param device:
        :param server:
        :return:
        """

        device_ip = config.get_data("device", device)
        server_ip = server
        num = 0
        found = False
        call_id = 'none'
        cseq = 'none'
        for pkt in cap:
            if pkt.highest_layer.find("SIP") != -1:
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is False:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('REGISTER') != -1 and cap[num]['sip'].expires != "0" and \
                                    cap[num]['sip'].from_user == user:
                                found = True
                                test.log("REGISTER request line: %s" % cap[num]['sip'].request_line)
                                call_id = cap[num]['sip'].call_id
                                cseq = cap[num]['sip'].cseq_seq
                                if 'authorization' in cap[num]['sip'].field_names:
                                    test.log("***** REGISTER sent with authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))
                                    test.log("REGISTER sent by User: {} with expires: {}".format(
                                        cap[num]['sip'].from_user, cap[num]['sip'].expires))
                                else:
                                    test.log("*****REGISTER sent without authorization, call-id: {}, "
                                                   "cseq: {}***** \n".format(call_id, cseq))
                                    test.log("REGISTER sent by User: {} with expires: {}".format(
                                        cap[num]['sip'].from_user, cap[num]['sip'].expires))

                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('401 Unauthorized') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = False
                                test.log("401 received for call-id: {}, cseq: {} \n".format(call_id, cseq))
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('200 OK') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("200 Ok received for call-id: {}, cseq: {} \n".format(call_id, cseq))
                                break
            num += 1
        if found is False:
            return found
        else:
            return found

    def verify_invite_sent(self, cap, device, server, ignore_packet=-1):
        """
        Function to verify INVITE is sent from the device
        :param cap: capture file
        :param device: device IP
        :param server: server IP
        :param ignore_packet: packet number of the current INVITE message
        :return:
        """
        device_ip = config.get_data("device", device)
        server_ip = server
        num = 0
        found = False
        call_id = None
        cseq = None
        for pkt in cap:
            if num > ignore_packet:
                if pkt.highest_layer.find("SIP") is not None:
                    if (cap[num]['ip'].src.find(device_ip) is not None and
                            cap[num]['ip'].dst.find(server_ip) is not None):
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method == 'INVITE':
                                found = True
                                test.log("INVITE request line: %s" % cap[num]['sip'].request_line)
                                call_id = cap[num]['sip'].call_id
                                cseq = cap[num]['sip'].cseq_seq
                                test.log("INVITE sent with call id: %s, cseq: %s \n" % (call_id,
                                                                                              cap[num]['sip'].cseq_seq))

            num += 1
        if found is False:
            test.log("ERROR: INVITE not sent")
            raise AssertionError("ERROR: INVITE not sent")
        else:
            return call_id, cseq, num

    def verify_invite_with_auth_sent(self, cap, device, server, ignore_packet=-1):
        """
        Function to verify INVITE is sent from the device
        :param cap: capture file
        :param device: device IP
        :param server: server IP
        :param ignore_packet: packet number of the current INVITE message
        :return:
        """
        # Get device IP and proxy server IP from config file
        device_ip = config.get_data("device", device)
        server_ip = server

        num = 0
        found = False
        call_id = 'none'
        cseq = 'none'
        for pkt in cap:
            if pkt.highest_layer.find("SIP") != -1:
                if num > ignore_packet:
                    if (cap[num]['ip'].src.find(device_ip) != -1 and
                            cap[num]['ip'].dst.find(server_ip) != -1):
                        if found is False:
                            if 'method' in cap[num]['sip'].field_names:
                                if cap[num]['sip'].method.find('INVITE') != -1:
                                    found = True
                                    test.log("INVITE request line: %s" % cap[num]['sip'].request_line)
                                    call_id = cap[num]['sip'].call_id
                                    cseq = cap[num]['sip'].cseq_seq
                                    if 'authorization' in cap[num]['sip'].field_names:
                                        test.log("=====INVITE sent with authorization, call-id: " + call_id +
                                                       " ,cseq: " + cseq + "======\n")
                                    else:
                                        test.log("=====INVITE sent without authorization, call-id: " + call_id +
                                                       " ,cseq: " + cseq + "======\n")

                    if (cap[num]['ip'].src.find(server_ip) != -1 and
                            cap[num]['ip'].dst.find(device_ip) != -1):
                        if found is True:
                            if 'status_line' in cap[num]['sip'].field_names:
                                if (cap[num]['sip'].status_line.find('401 Unauthorized') != -1 and
                                        cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                    found = False
                                    test.log("401 received for callid: " + call_id + " ,cseq: " + cseq)
                                elif (cap[num]['sip'].status_line.find('180 Ringing') != -1 and
                                      cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                    found = True
                                    test.log("180 received for callid: " + call_id + " ,cseq: " + cseq)
                                    break
                                elif (cap[num]['sip'].status_line.find('183 Session') != -1 and
                                      cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                    found = True
                                    test.log("183 Session Progress received for callid: " + call_id + " ,cseq: " + cseq)
                                    break
            num += 1
        if found is False:
            test.log("ERROR: INVITE not sent")
            raise AssertionError("ERROR: INVITE not sent")
        else:
            print "****** Call id: {} \n ****** Cseq: {} \n, ****** pkt num: {}".format(call_id, cseq, num)
            return call_id, cseq, num

    def verify_ack_sent(self, cap, device, server, invite_call_id):

        device_ip = config.get_data("device", device)
        server_ip = server
        found = False
        num = 0
        for pkt in cap:
            if pkt.highest_layer.find("SIP") != -1:
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if 'method' in cap[num]['sip'].field_names:
                        if cap[num]['sip'].method.find('ACK') != -1:
                            call_id = cap[num]['sip'].call_id
                            if invite_call_id.find(call_id) != -1:
                                found = True
                                test.log("request line: %s" % cap[num]['sip'].request_line)
                                test.log("ACK sent with call id %s" % call_id)

                            else:
                                test.log('ACK call id different: %s' % call_id)
            num += 1
        if found is False:
            test.log("ERROR: ACK not sent")
            raise AssertionError("ERROR: ACK not sent")

    def verify_100_trying_received(self, cap, device, server, cseq, call_id, ignore_packet=-1):
        """
        Function to check 100 Trying received by the device
        :param cap:
        :param device:
        :param server:
        :param cseq:
        :param call_id:
        :param ignore_packet:
        :return:
        """

        device_ip = config.get_data("device", device)
        server_ip = server
        num = 0
        found = False
        print "****** Call id: {} \n Cseq: {}".format(call_id, cseq)
        for pkt in cap:
            if num > ignore_packet:
                if pkt.highest_layer.find("SIP") is not None:
                    if (cap[num]['ip'].src.find(server_ip) is not None and
                            cap[num]['ip'].dst.find(device_ip) is not None):
                        if 'status_code' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].status_code == '100':
                                if (cap[num]['sip'].call_id == call_id and
                                        cap[num]['sip'].cseq_seq == cseq):
                                    found = True
                                    print ("Found 100 Trying")
            num += 1
        if found is False:
            print("ERROR: 100 Trying not received")
            raise AssertionError("ERROR: 100 Trying not received")

    def verify_180_ringing_received(self, cap, device, server, cseq, call_id, ignore_packet=-1):
        """
        Function to check 180 Ringing is received by the device
        :param cap:
        :param device:
        :param server:
        :param cseq:
        :param call_id:
        :param ignore_packet:
        :return:
        """

        device_ip = config.get_data("device", device)
        server_ip = server

        num = 0
        found = False
        print "****** Call id: {} \n Cseq: {}".format(call_id, cseq)
        for pkt in cap:
            if num > ignore_packet:
                if pkt.highest_layer.find("SIP") is not None:
                    if (cap[num]['ip'].src.find(server_ip) is not None and
                            cap[num]['ip'].dst.find(device_ip) is not None):
                        if 'status_code' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].status_code == '180':
                                if (cap[num]['sip'].call_id == call_id and
                                        cap[num]['sip'].cseq_seq == cseq):
                                    found = True
                                    print ("Found 180 Ringing")
            num += 1
        if found is False:
            print("ERROR: 180 Ringing not received")
            raise AssertionError("ERROR: 180 Ringing not received")

    def verify_200_received(self, cap, device, server, cseq, call_id, ignore_packet=-1):
        """
        Function to check 200 OK is received by the device for INVITE sent
        :param cap:
        :param device:
        :param server:
        :param cseq:
        :param call_id:
        :param ignore_packet:
        :return:
        """
        print "****** Call id: {} \n Cseq: {}".format(call_id, cseq)
        device_ip = config.get_data("device", device)
        server_ip = server
        num = 0
        found = False
        for pkt in cap:
            if num > ignore_packet:
                if pkt.highest_layer.find("SIP") is not None:
                    if (cap[num]['ip'].src.find(server_ip) is not None and
                            cap[num]['ip'].dst.find(device_ip) is not None):
                        if 'status_code' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].status_code == '200':
                                if (cap[num]['sip'].call_id == call_id and
                                        cap[num]['sip'].cseq_seq == cseq):
                                    found = True
                                    print ("Found 200 OK")
            num += 1
        if found is False:
            print("ERROR: 200 OK not received")
            raise AssertionError("ERROR: 200 OK not received")

    def verify_ack_sent_1(self, cap, device, server, cseq, call_id, ignore_packet=-1):
        """
        Function to check ACK is sent bu the device after getting 200 OK response
        :param cap:
        :param device:
        :param server:
        :param cseq:
        :param call_id:
        :param ignore_packet:
        :return:
        """
        print "****** Call id: {} \n Cseq: {}".format(call_id, cseq)
        device_ip = config.get_data("device", device)
        server_ip = server
        num = 0
        found = False
        for pkt in cap:
            if num > ignore_packet:
                if pkt.highest_layer.find("SIP") is not None:
                    if (cap[num]['ip'].src.find(device_ip) is not None and
                            cap[num]['ip'].dst.find(server_ip) is not None):
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('ACK'):
                                if cseq.find(cap[num]['sip'].cseq_seq):
                                    found = True
                                    print("ACK sent for INVITE with call id: %s, cseq: %s \n"
                                          % (call_id, cap[num]['sip'].cseq_seq))
        num += 1
        if found is False:
            print("ERROR: ACK not sent")
            raise AssertionError("ERROR: ACK not sent")

    def verify_outbound_call_flow(self, cap, device, server):
        """

        :param cap:
        :param device:
        :param server:
        :return:
        """

        device_ip = config.get_data("device", device)
        server_ip = server
        num = 0
        found = False
        call_id = 'none'
        cseq = 'none'
        for pkt in cap:
            if pkt.highest_layer.find("SIP") != -1:
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is False:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('INVITE') != -1:
                                found = True
                                test.log("INVITE request line: %s" % cap[num]['sip'].request_line)
                                call_id = cap[num]['sip'].call_id
                                cseq = cap[num]['sip'].cseq_seq
                                if 'authorization' in cap[num]['sip'].field_names:
                                    test.log("***** INVITE sent with authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))
                                else:
                                    test.log("***** INVITE sent without authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))

                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('401 Unauthorized') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = False
                                test.log("401 received for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('180 Ringing') != -1 and
                                  cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("180 received for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('183 Session') != -1 and
                                  cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("183 Session Progress received for call-id: {}, cseq: {}".format(
                                    call_id, cseq))
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('200 OK') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("200 Ok received for call-id: {}, cseq: {}".format(call_id, cseq))
                                # break
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('ACK') != -1:
                                if cseq == cap[num]['sip'].cseq_seq:
                                    found = True
                                    test.log("ACK is sent for INVITE: %s" % call_id)
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('BYE') != -1:
                                if call_id == cap[num]['sip'].call_id:
                                    found = True
                                    test.log("BYE is sent for INVITE: %s" % call_id)
                elif (cap[num]['ip'].src.find(server_ip) != -1 and
                      cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('BYE') != -1:
                                if call_id == cap[num]['sip'].call_id:
                                    found = True
                                    test.log("BYE is received for INVITE: %s" % call_id)
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('200 OK') != -1 and
                                    call_id == cap[num]['sip'].call_id):
                                if cap[num]['sip'].cseq_method == 'BYE':
                                    found = True
                                    test.log("200 Ok for BYE is sent for call-id: {}, cseq: {}\n".format(
                                        call_id, cseq))
                                    break
                elif (cap[num]['ip'].src.find(server_ip) != -1 and
                      cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('200 OK') != -1 and
                                    call_id == cap[num]['sip'].call_id):
                                if cap[num]['sip'].cseq_method == 'BYE':
                                    found = True
                                    test.log("200 Ok for BYE is sent for call-id: {}, cseq: {}\n".format(
                                        call_id, cseq))
                                    break

            num += 1
        if found is False:
            test.log("ERROR: INVITE not sent")
            return False
        else:
            return True

    def verify_inbound_call_flow(self, cap, device, server):
        """

        :param cap:
        :param device:
        :param server:
        :return:
        """

        device_ip = config.get_data("device", device)
        server_ip = server
        num = 0
        found = False
        call_id = 'none'
        cseq = 'none'
        for pkt in cap:
            if pkt.highest_layer.find("SIP") != -1:
                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is False:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('INVITE') != -1:
                                found = True
                                test.log("INVITE request line: %s" % cap[num]['sip'].request_line)
                                call_id = cap[num]['sip'].call_id
                                cseq = cap[num]['sip'].cseq_seq
                                if 'authorization' in cap[num]['sip'].field_names:
                                    test.log("***** INVITE received with authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))
                                else:
                                    test.log("***** INVITE received without authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))

                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('401 Unauthorized') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = False
                                test.log("401 sent for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('180 Ringing') != -1 and
                                  cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("180 sent for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('183 Session') != -1 and
                                  cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("183 Session Progress sent for call-id: {}, cseq: {}".format(call_id,
                                                                                                            cseq))
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('200 OK') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("200 Ok sent for call-id: {}, cseq: {}".format(call_id, cseq))
                                # break
                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('ACK') != -1:
                                if cseq == cap[num]['sip'].cseq_seq:
                                    found = True
                                    test.log("ACK is received for INVITE: %s" % call_id)
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('BYE') != -1:
                                if call_id == cap[num]['sip'].call_id:
                                    found = True
                                    test.log("BYE is sent for INVITE: %s" % call_id)
                elif (cap[num]['ip'].src.find(server_ip) != -1 and
                      cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('BYE') != -1:
                                if call_id == cap[num]['sip'].call_id:
                                    found = True
                                    test.log("BYE is received for INVITE: %s" % call_id)
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('200 OK') != -1 and
                                    call_id == cap[num]['sip'].call_id):
                                if cap[num]['sip'].cseq_method == 'BYE':
                                    found = True
                                    test.log("200 Ok for BYE is sent for call-id: {}, cseq: {}\n".format(
                                        call_id, cseq))
                                    break
                elif (cap[num]['ip'].src.find(server_ip) != -1 and
                      cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('200 OK') != -1 and
                                    call_id == cap[num]['sip'].call_id):
                                if cap[num]['sip'].cseq_method == 'BYE':
                                    found = True
                                    test.log("200 Ok for BYE is received for call-id: {}, cseq: {}\n".format(
                                        call_id, cseq))
                                    break

            num += 1
        if found is False:
            test.log("ERROR: INVITE not sent")
            return found
        else:
            return True

    def verify_inbound_cancel_call_flow(self, cap, device, server):
        """

        :param cap:
        :param device:
        :param server:
        :return:
        """

        device_ip = config.get_data("device", device)
        server_ip = server
        num = 0
        found = False
        call_id = 'none'
        cseq = 'none'
        for pkt in cap:
            if pkt.highest_layer.find("SIP") != -1:
                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is False:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('INVITE') != -1:
                                found = True
                                test.log("INVITE request line: %s\n" % cap[num]['sip'].request_line)
                                call_id = cap[num]['sip'].call_id
                                cseq = cap[num]['sip'].cseq_seq
                                if 'authorization' in cap[num]['sip'].field_names:
                                    test.log("***** INVITE received with authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))
                                else:
                                    test.log("***** INVITE received without authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))

                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('401 Unauthorized') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = False
                                test.log("401 sent for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('180 Ringing') != -1 and
                                  cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("180 sent for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('183 Session') != -1 and
                                  cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("183 Session Progress sent for call-id: {}, cseq: {}".format(call_id,
                                                                                                            cseq))
                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('CANCEL') != -1:
                                if cseq == cap[num]['sip'].cseq_seq:
                                    found = True
                                    test.log("CANCEL is received for INVITE: %s" % call_id)
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('487') != -1 and
                                    cap[num]['sip'].cseq_method == 'INVITE'):
                                found = True
                                test.log("487 Request Terminated sent for call-id: {}, cseq: {}".format(
                                    call_id, cseq))
                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('ACK') != -1:
                                if cseq == cap[num]['sip'].cseq_seq:
                                    found = True
                                    test.log("ACK is received for INVITE: %s" % call_id)
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('200 OK') != -1 and
                                    call_id == cap[num]['sip'].call_id):
                                if cap[num]['sip'].cseq_method == 'CANCEL':
                                    found = True
                                    test.log("200 Ok for CANCEL is sent for call-id: {}, cseq: {}\n".format(
                                        call_id, cseq))

            num += 1
        if found is False:
            test.log("ERROR: CANCEL Call flow for Inbound Call failed")
            return found
        else:
            return found

    def verify_outbound_cancel_call_flow(self, cap, device, server):
        """

        :param cap:
        :param device:
        :param server:
        :return:
        """

        device_ip = config.get_data("device", device)
        server_ip = server
        num = 0
        found = False
        call_id = 'none'
        cseq = 'none'
        for pkt in cap:
            if pkt.highest_layer.find("SIP") != -1:
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is False:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('INVITE') != -1:
                                found = True
                                test.log("INVITE request line: %s" % cap[num]['sip'].request_line)
                                call_id = cap[num]['sip'].call_id
                                cseq = cap[num]['sip'].cseq_seq
                                if 'authorization' in cap[num]['sip'].field_names:
                                    test.log("***** INVITE sent with authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))
                                else:
                                    test.log("***** INVITE sent without authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))

                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('401 Unauthorized') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = False
                                test.log("401 received for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('180 Ringing') != -1 and
                                  cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("180 received for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('183 Session') != -1 and
                                  cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("183 Session Progress received for call-id: {}, cseq: {}".format(
                                    call_id, cseq))
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('CANCEL') != -1:
                                if cseq == cap[num]['sip'].cseq_seq:
                                    found = True
                                    test.log("CANCEL is sent for INVITE: %s" % call_id)
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('ACK') != -1:
                                if cseq == cap[num]['sip'].cseq_seq:
                                    found = True
                                    test.log("ACK is sent for INVITE: %s" % call_id)
                if cap[num]['ip'].src.find(server_ip) != -1 and cap[num]['ip'].dst.find(device_ip) != -1:
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('487') != -1 and
                                    call_id == cap[num]['sip'].call_id):
                                if cap[num]['sip'].cseq_method == 'INVITE':
                                    found = True
                                    test.log("487 Request Terminated is received for call-id: {}, "
                                                   "cseq: {}".format(call_id, cseq))
                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('200 OK') != -1 and
                                    call_id == cap[num]['sip'].call_id):
                                if cap[num]['sip'].cseq_method == 'CANCEL':
                                    found = True
                                    test.log("200 Ok for CANCEL is received for call-id: {}, cseq: {}\n".format(
                                        call_id, cseq))
                                    break
            num += 1
        if found is False:
            test.log("ERROR: CANCEL Call flow for Outbound Call failed")
            return found
        else:
            return found

    def verify_outbound_reject_call_flow(self, cap, device, server):
        """

        :param cap:
        :param device:
        :param server:
        :return:
        """

        device_ip = config.get_data("device", device)
        server_ip = server
        num = 0
        found = False
        call_id = 'none'
        cseq = 'none'
        for pkt in cap:
            if pkt.highest_layer.find("SIP") != -1:
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is False:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('INVITE') != -1:
                                found = True
                                test.log("INVITE request line: %s" % cap[num]['sip'].request_line)
                                call_id = cap[num]['sip'].call_id
                                cseq = cap[num]['sip'].cseq_seq
                                if 'authorization' in cap[num]['sip'].field_names:
                                    test.log("***** INVITE sent with authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))
                                else:
                                    test.log("***** INVITE sent without authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))

                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('401 Unauthorized') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = False
                                test.log("401 received for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('180 Ringing') != -1 and
                                  cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("180 received for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('183 Session') != -1 and
                                  cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("183 Session Progress received for call-id: {}, cseq: {}".format(
                                    call_id, cseq))
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('486 Busy') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("486 Busy Here received for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('480') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("480 Temporarily Not Available received for call-id: {}, "
                                               "cseq: {}".format(call_id, cseq))
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('ACK') != -1:
                                if cseq == cap[num]['sip'].cseq_seq:
                                    found = True
                                    test.log("ACK is sent for INVITE: %s \n" % call_id)
                                    break
            num += 1
        if found is False:
            test.log("ERROR: INVITE not sent")
            return found
        else:
            return found

    def verify_inbound_reject_call_flow(self, cap, device, server):
        """

        :param cap:
        :param device:
        :param server:
        :return:
        """

        device_ip = config.get_data("device", device)
        server_ip = server
        num = 0
        found = False
        call_id = 'none'
        cseq = 'none'
        for pkt in cap:
            if pkt.highest_layer.find("SIP") != -1:
                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is False:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('INVITE') != -1:
                                found = True
                                test.log("INVITE request line: %s" % cap[num]['sip'].request_line)
                                call_id = cap[num]['sip'].call_id
                                cseq = cap[num]['sip'].cseq_seq
                                if 'authorization' in cap[num]['sip'].field_names:
                                    test.log("***** INVITE received with authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))
                                else:
                                    test.log("***** INVITE received without authorization, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))

                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('401 Unauthorized') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = False
                                test.log("401 sent for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('180 Ringing') != -1 and
                                  cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("180 sent for call-id: {}, cseq: {}".format(call_id, cseq))
                            elif (cap[num]['sip'].status_line.find('183 Session') != -1 and
                                  cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("183 Session Progress sent for call-id: {}, cseq: {}".format(call_id,
                                                                                                            cseq))
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if (cap[num]['sip'].status_line.find('480') != -1 and
                                    cseq.find(cap[num]['sip'].cseq_seq) != -1):
                                found = True
                                test.log("480 Temporarily Unavailable sent for call-id: {}, cseq: {}".format(
                                    call_id, cseq))
                                # break
                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('ACK') != -1:
                                if cseq == cap[num]['sip'].cseq_seq:
                                    found = True
                                    test.log("ACK is received for INVITE: %s\n" % call_id)
                                    break
            num += 1
        if found is False:
            test.log("ERROR: INVITE not sent")
            return found
        else:
            return found

    def verify_hold_flow(self, cap, device, server):
        """

        :param cap:
        :param device:
        :param server:
        :return:
        """

        device_ip = config.get_data("device", device)
        server_ip = server

        pattern1 = re.compile('sendonly')
        pattern2 = re.compile('recvonly')
        num = 0
        found = False
        call_id = 'none'
        cseq = 'none'
        for pkt in cap:
            if pkt.highest_layer.find("SIP") != -1:
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is False:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('INVITE') != -1:
                                data = cap[num]['sip'].msg_hdr
                                finding = re.findall(pattern1, data)
                                if finding == ['sendonly']:
                                    found = True
                                    call_id = cap[num]['sip'].call_id
                                    cseq = cap[num]['sip'].cseq_seq
                                    test.log("*********** Hold INVITE Sent ************")
                                    test.log("***** Hold INVITE sent with, call-id: {}, "
                                                   "cseq: {} *****\n".format(call_id, cseq))
                if (cap[num]['ip'].src.find(server_ip) != -1 and
                        cap[num]['ip'].dst.find(device_ip) != -1):
                    if found is True:
                        if 'status_line' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].status_line.find('200 OK') != -1:
                                if cseq == cap[num]['sip'].cseq_seq:
                                    data = cap[num]['sip'].msg_hdr
                                    finding = re.findall(pattern2, data)
                                    if finding == ['recvonly']:
                                        found = True
                                        test.log("Held 200 Ok received for call-id: {}, "
                                                       "cseq: {}".format(call_id, cseq))
                if (cap[num]['ip'].src.find(device_ip) != -1 and
                        cap[num]['ip'].dst.find(server_ip) != -1):
                    if found is True:
                        if 'method' in cap[num]['sip'].field_names:
                            if cap[num]['sip'].method.find('ACK') != -1 and \
                                    call_id == cap[num]['sip'].call_id:
                                found = True
                                test.log("ACK is sent for INVITE with call-id: {}, "
                                               "cseq: {}".format(call_id, cseq))
                                break
            num += 1
        if found is False:
            test.log("ERROR: Hold INVITE not sent")
            return found
        else:
            return num, found

    def verify_resume_flow(self, cap, device, server, ignore_pkt=-1):
        """

        :param cap:
        :param device:
        :param server:
        :param ignore_pkt:
        :return:
        """

        device_ip = config.get_data("device", device)
        server_ip = server

        num = 0
        pattern = re.compile('sendrecv')
        found = False
        call_id = 'none'
        cseq = 'none'
        for pkt in cap:
            if num > ignore_pkt:
                if pkt.highest_layer.find("SIP") != -1:
                    if (cap[num]['ip'].src.find(device_ip) != -1 and
                            cap[num]['ip'].dst.find(server_ip) != -1):
                        if found is False:
                            if 'method' in cap[num]['sip'].field_names:
                                if cap[num]['sip'].method.find('INVITE') != -1:
                                    data = cap[num]['sip'].msg_hdr
                                    finding = re.findall(pattern, data)
                                    if finding == ['sendrecv']:
                                        call_id = cap[num]['sip'].call_id
                                        cseq = cap[num]['sip'].cseq_seq
                                        found = True
                                        test.log("************ Resume INVITE Sent ************")
                                        test.log("***** Resume INVITE sent with, call-id: {}, "
                                                       "cseq: {} *****\n".format(call_id, cseq))
                    if (cap[num]['ip'].src.find(server_ip) != -1 and
                            cap[num]['ip'].dst.find(device_ip) != -1):
                        if found is True:
                            if 'status_line' in cap[num]['sip'].field_names:
                                if (cap[num]['sip'].status_line.find('200 OK') != -1 and
                                        call_id == cap[num]['sip'].call_id):
                                    found = True
                                    test.log("Resume 200 Ok received for call-id: {}, "
                                                   "cseq: {}".format(call_id, cseq))
                    if (cap[num]['ip'].src.find(device_ip) != -1 and
                            cap[num]['ip'].dst.find(server_ip) != -1):
                        if found is True:
                            if 'method' in cap[num]['sip'].field_names:
                                if cap[num]['sip'].method.find('ACK') != -1 and call_id == cap[num]['sip'].call_id:
                                    found = True
                                    test.log("ACK is sent for INVITE with call-id: {}, "
                                                   "cseq: {}".format(call_id, cseq))
                                    break
            num += 1
        if found is False:
            test.log("ERROR: Resume INVITE not sent")
            return found
        else:
            return found


