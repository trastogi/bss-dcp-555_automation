"""
This Page Object module contains the page classes for different configurations
can be done on the device
"""
# source(findFile("scripts", "/device/PageObject/PageObject.py"))
source(findFile("scripts", "/device/PageObject/PageObject.py"))
# from PageObject import PageObject
from selenium.common.exceptions import NoSuchElementException

global config, driver, log


class VoipSettings(PageObject):
    """
    Page object class for VoIP Settings
    """

    def __init__(self):
#         super(PageObject, self).__init__(self)

        FORM = "//*[@href='/#/settings/voipsettings']"
        
        if driver.find_element_by_xpath(FORM):
            log.write("VoIP Settings page opened")
        else:
            raise AssertionError("Unable to open VoIP Settings Page")

        # Xpath for VoiP settings page
        self.general_voip_line_settings = "//*[@href='#GeneralVoIP']"
        self.voip_line_setting = "//*[@href='#voipline{}']"
        self.voip_line_1_setting = "//*[@href='#voipline0']"
        self.voip_line_2_setting = "//*[@href='#voipline1']"
        self.voip_line_3_setting = "//*[@href='#voipline2']"
        self.voip_line_4_setting = "//*[@href='#voipline3']"
        self.tab_heading = "//*[@class='net_heading']"
        self.transport_type = "//*[@class='dropdown']"
        self._username = "(//input[@type='text'])[2]"
        self._auth_name = "(//input[@type='text'])[3]"
        self._voip_password = "//input[@type='password']"
        self._display_name = "(//input[@type='text'])[4]"
        self._voip_domain = "(//input[@type='text'])[5]"
        self._account_status = "//*[contains(text(),'{}')]".format('Registered')
        self._launch_dialer = "//*[@value='Launch Dialer and Volume Settings']"
        self._close_dialer = "//*[@class='close_buttom']"
        self._dialer_digits = "//app-dialer-key[{}]"

        # Css_selector for VoIP settings page
        self.transport = "div.voip_net:nth-child(1) > div:nth-child(1) > label:nth-child(1)"
        self.auto = ".dropdown-menu > li:nth-child(1) > a:nth-child(1)"
        self.tcp = ".dropdown-menu > li:nth-child(2) > a:nth-child(1)"
        self.tls = ".dropdown-menu > li:nth-child(3) > a:nth-child(1)"
        self.udpOnly = ".dropdown-menu > li:nth-child(4) > a:nth-child(1)"
        self.rport = "div.voip_net:nth-child(2) > div:nth-child(2) > input:nth-child(1)"
        self.t1 = "div.voip_net:nth-child(3) > div:nth-child(2) > input:nth-child(1)"
        self.t2 = "div.voip_net:nth-child(4) > div:nth-child(2) > input:nth-child(1)"
        self.t4 = "div.voip_net:nth-child(5) > div:nth-child(2) > input:nth-child(1)"
        self.td = "div.voip_net:nth-child(6) > div:nth-child(2) > input:nth-child(1)"
        self.stun_port = "div.col-md-12:nth-child(7) > div:nth-child(2) > input:nth-child(1)"
        self.stun_server = "div.col-md-12:nth-child(8) > div:nth-child(2) > input:nth-child(1)"
        self._use_domain_switch = "div.col-md-8:nth-child(8) > div:nth-child(2) > label:nth-child(1)"
        self._proxy_address = "div.col-md-12:nth-child(10) > div:nth-child(2) > input:nth-child(1)"
        self._dialer_heading = "span.col-lg-2:nth-child(1)"
        self._call_button = ".oncall-box"
        self._end_button = ".hangup"
        self._accept_button = "div.incoming-call-reponse:nth-child(1)"
        self._reject_button = "div.incoming-call-reponse:nth-child(2)"
        self.mute = "div.col-lg-4:nth-child(5)"
        self.hold_button = "div.ptop10-px:nth-child(1)"
        self.resume_button = "div.ptop10-px:nth-child(2)"
        self.redial_button = ".mtop30-px > div:nth-child(3)"
        self.auto_answer_button = ".auto-answer-btn"
        self.dnd_disabled = ".dnd-btn"
        self.dnd_enabled = ".dnd-btn.dnd-on"

    # def navigate_to_general_voip_line_settings(self):
    #     """
    #     Function to navigate to General VoIP Line Settings
    #     :return:
    #     """
    #     self._navigate_to_lines(self.general_voip_line_settings, 0)
    #
    # def navigate_to_voip_line_1_settings(self):
    #     """
    #     Function to navigate to VoIP Line 1 Settings
    #     :return:
    #     """
    #     self._navigate_to_lines(self.voip_line_1_setting, 1)
    #
    # def navigate_to_voip_line_2_settings(self):
    #     """
    #     Function to navigate to VoIP Line 2 Settings
    #     :return:
    #     """
    #     self._navigate_to_lines(self.voip_line_2_setting, 2)
    #
    # def navigate_to_voip_line_3_settings(self):
    #     """
    #     Function to navigate to VoIP Line 3 Settings
    #     :return:
    #     """
    #     self._navigate_to_lines(self.voip_line_3_setting, 3)
    #
    # def navigate_to_voip_line_4_settings(self):
    #     """
    #     Function to navigate to VoIP Line 4 Settings
    #     :return:
    #     """
    #     self._navigate_to_lines(self.voip_line_4_setting, 4)
    #
    # def _navigate_to_lines(self, xpath, line_no):
    #     """
    #     Function to navigate between lines in VoIP Settings Page
    #     :return:
    #     """
    #     self.driver.find_element_by_xpath(xpath).click()
    #     if line_no == 0:
    #         if self.driver.find_element_by_css_selector(self.transport):
    #             self.log.write("Successfully navigated to General VoIP Line Settings")
    #             return True
    #     elif 1 <= line_no <= 4:
    #         page_name = self.driver.find_element_by_xpath(self.tab_heading).text
    #         if page_name == "VoIP Line {} - Protocol".format(str(line_no)):
    #             self.log.write("Successfully navigated to VoIP Line {} Settings".format(str(line_no)))
    #             return True
    #         else:
    #             self.log.write("We are in wrong page")
    #             return False
    #     else:
    #         self.log.write("Line number should between 0 to 4, where 0 is for General VoIP settings, and 1 to 4 is "
    #                        "respective lines", level="error")

    def navigate_to_line(self, line_no):
        """
        Function to navigate between lines in VoIP Settings Page
        :return:
        """
        if 0 <= line_no <= 4:
            if line_no == 0:
                driver.find_element_by_xpath(self.general_voip_line_settings).click()
                if driver.find_element_by_css_selector(self.transport):
                    test.log("Successfully navigated to General VoIP Line Settings")
            elif 1 <= line_no <= 4:
                driver.find_element_by_xpath(self.voip_line_setting.format(str(line_no - 1))).click()
                page_name = driver.find_element_by_xpath(self.tab_heading).text
                if page_name == "VoIP Line {} - Protocol".format(str(line_no)):
                    test.log("Successfully navigated to VoIP Line {} Settings".format(str(line_no)))
                else:
                    test.log("Unable to Navigate")
        elif len(line_no) == 5 or 1:
            if len(line_no) == 5:
                line_no = line_no[-1]
                if line_no == "1":
                    line_no = 1
                elif line_no == "2":
                    line_no = 2
                elif line_no == "3":
                    line_no = 3
                elif line_no == "4":
                    line_no = 4
            elif len(line_no) == 1:
                line_no = line_no
                if line_no == "1":
                    line_no = 1
                elif line_no == "2":
                    line_no = 2
                elif line_no == "3":
                    line_no = 3
                elif line_no == "4":
                    line_no = 4
            driver.find_element_by_xpath(self.voip_line_setting.format(str(line_no - 1))).click()
            page_name = driver.find_element_by_xpath(self.tab_heading).text
            if page_name == "VoIP Line {} - Protocol".format(str(line_no)):
                test.log("Successfully navigated to VoIP Line {} Settings".format(str(line_no)))
            else:
                test.log("Unable to Navigate")
        elif line_no.lower() == "general":
            driver.find_element_by_xpath(self.general_voip_line_settings).click()
            if driver.find_element_by_css_selector(self.transport):
                test.log("Successfully navigated to General VoIP Line Settings")
        else:
            test.log("Line number should between 0 to 4, where 0 is for General VoIP settings, and 1 to 4 is "
                           "respective lines", level="error")
    
    def set_voip_transport_method(self, transport="auto", ):
        """
        Function to select Transport Type on General VoIP Line Settings page
        :param transport: select the transport method, can be "auto", "tcp", "tls", "udpOnly"
        :return:
        """
        transport = transport.lower()
        driver.find_element_by_xpath(self.transport_type).click()
        if transport == "auto":
            driver.find_element_by_css_selector(self.auto).click()
            test.log("Selected 'Auto' as Transport type")
        elif transport == "tcp":
            driver.find_element_by_css_selector(self.tcp).click()
            test.log("Selected 'TCP' as Transport type")
        elif transport == "tls":
            driver.find_element_by_css_selector(self.tls).click()
            test.log("Selected 'TLS' as Transport type")
        elif transport == "udponly":
            driver.find_element_by_css_selector(self.udpOnly).click()
            test.log("Selected 'UDPOnly' as Transport type")
        else:
            test.warning("Wrong selection of Transport Type")

    def configure_general_voip_settings(self, rport=None, t1=None, t2=None, t4=None, td=None):
        """
        Function to configure parameters on General VoIP Line Settings Page
        :param rport: can be set between 0 to 65535, 0 is default
        :param t1: can be set between 100 to 10000, 500 is default
        :param t2: can be set between 100 to 10000, 4000 is default
        :param t4: can be set between 100 to 10000, 5000 is default
        :param td: can be set between 400 to 60000, 32000 is default
        :return:
        """
        if rport is not None and 0 <= int(rport) <= 65535:
            test.log("Setting RPort value to {}".format(rport))
            self.set_value_on_web(self.rport, rport, selector="css_selector")
        if t1 is not None and 100 <= int(t1) <= 10000:
            test.log("Setting T1 Timer value to {}".format(t1))
            self.set_value_on_web(self.t1, t1, selector="css_selector")
        if t2 is not None and 100 <= int(t2) <= 10000:
            test.log("Setting T2 Timer value to {}".format(t2))
            self.set_value_on_web(self.t2, t2, selector="css_selector")
        if t4 is not None and 100 <= int(t4) <= 10000:
            test.log("Setting T4 Timer value to {}".format(t4))
            self.set_value_on_web(self.t4, t4, selector="css_selector")
        if td is not None and 400 <= int(td) <= 60000:
            test.log("Setting TD Timer value to {}".format(td))
            self.set_value_on_web(self.td, td, selector="css_selector")

    def set_stun_settings(self, stun_server="", stun_port=None):
        """
        Function to set STUN server settings on General VoIP Line Settings page
        :param stun_port:
        :param stun_server:
        :return:
        """
        if stun_server != "":
            test.log("Setting STUN Server address to {}".format(stun_server))
            self.set_value_on_web(self.stun_server, stun_server, selector="css_selector")
        if stun_port is not None and 0 <= int(stun_port) <= 65535:
            test.log("Setting STUN Port value to {}".format(stun_port))
            self.set_value_on_web(self.stun_port, stun_port, selector="css_selector")

    def set_register_parameters(self, username="", auth_name="", password="", display_name="", domain="", proxy="",
                                auth_enable=True, use_domain=True):
        """
        Function to set parameters necessary for Registering the device
        :param username: Directory Number configured for device,
        :param auth_name: Authentication Name
        :param password: VoIP Password
        :param display_name: Display Name for the DN
        :param domain: Proxy server address, CUCM or Asterisk server address
        :param proxy: Proxy server address, CUCM or Asterisk server address
        :param auth_enable: Switch for enable/disable authentication
        :param use_domain: Switch for decision to use Domain or Proxy
        :return:
        """
        test.log("Setting Username to {}".format(username))
        self.set_value_on_web(self._username, username, selector="xpath")

        # Decision for setting Authentication parameters
        if auth_enable is True:
            test.log("Setting Authentication Name to {}".format(auth_name))
            self.set_value_on_web(self._auth_name, auth_name, selector="xpath")
            test.log("Setting Password to {}".format(password))
            self.set_value_on_web(self._voip_password, password, selector="xpath")

        test.log("Setting Display Name to {}".format(display_name))
        self.set_value_on_web(self._display_name, display_name, selector="xpath")

        # Decision for registration is done using Domain or Proxy address
        if use_domain is True:
            test.log("Setting Domain Name to {}".format(domain))
            self.set_value_on_web(self._voip_domain, domain, selector="xpath")
        else:
            self.register_with_domain_switch()
            test.log("Setting Proxy Address to {}".format(proxy))
            self.set_value_on_web(self._proxy_address, proxy, selector="css_selector")

    def register_with_domain_switch(self):
        """
        Function to toggle "Register with Domain" switch
        :return:
        """
        driver.find_element_by_css_selector(self._use_domain_switch).click()

    def launch_dialer(self):
        """
        Function to Launch Dialer and Volume settings on VoIP Line 1-4 Settings page
        :return:
        """
        driver.find_element_by_xpath(self._launch_dialer).click()
        if driver.find_element_by_css_selector(self._dialer_heading).is_displayed():
            test.log("Successfully Launched dialer")
            return True
        else:
            test.log("Failed to launch dialer")
            return False

    def close_dialer(self):
        """
        Function to close dialer window
        :return:
        """
        driver.find_element_by_xpath(self._close_dialer).click()
        if driver.find_element_by_css_selector(self._dialer_heading).is_displayed() is False:
            test.log("Successfully Closed dialer")
            return True
        else:
            test.log("Failed to Close dialer")
            return False

    def dial(self, number):
        """
        Function to dial number
        :param number:
        :return:
        """

        for i in range(len(number)):
            num = number[i]
            test.log("Dialing: {}".format(num))
            if num == "*":
                driver.find_element_by_xpath(self._dialer_digits.format("10")).click()
            elif num == "0":
                driver.find_element_by_xpath(self._dialer_digits.format("11")).click()
            elif num == "#":
                driver.find_element_by_xpath(self._dialer_digits.format("12")).click()
            else:
                driver.find_element_by_xpath(self._dialer_digits.format(num)).click()

    def press_call_button(self):
        """
        Function to press call button on dialer screen
        :return:
        """
        driver.find_element_by_css_selector(self._call_button).click()
        test.log("Pressing Call button")

    def press_end_call_button(self):
        """
        Function to press Call End button
        :return:
        """
        driver.find_element_by_css_selector(self._end_button).click()
        test.log("Pressing Call End button")

    def accept_call(self):
        """
        Function to press accept button on voip dialer page
        :return:
        """
        driver.find_element_by_css_selector(self._accept_button).click()
        test.log("Pressing Accept button")

    def reject_call(self):
        """
        Function to press reject button on voip dialer page
        :return:
        """
        driver.find_element_by_css_selector(self._reject_button).click()
        test.log("Pressing Reject button")

    def press_mute_button(self):
        """
        Function to press Mute button on VoIP dialer page
        :return:
        """
        driver.find_element_by_css_selector(self.mute).click()
        test.log("Clicking on Mute Button")

    def press_unmute_button(self):
        """
        Function to press UnMute button on VoIP dialer page
        :return:
        """
        if self.verify_unmute_button_is_enabled():
            driver.find_element_by_css_selector(self.mute).click()
            test.log("Clicking on UnMute button")

    def press_hold_button(self):
        """
        Function to press Hold button on VoIP dialer page
        :return:
        """
        driver.find_element_by_css_selector(self.hold_button).click()
        test.log("Clicking on Hold button")

    def press_resume_button(self):
        """
        Function to press Hold button on VoIP dialer page
        :return:
        """
        driver.find_element_by_css_selector(self.resume_button).click()
        test.log("Clicking on Hold button")

    def press_redial_button(self):
        """
        Function to press Redial Button
        :return:
        """
        driver.find_element_by_css_selector(self.redial_button).click()
        test.log("Clicking on Redial button")

    def enable_auto_answer(self):
        """
        Function to enable Auto Answer
        :return:
        """
        button = driver.find_element_by_css_selector(self.auto_answer_button)
        if button.text == "OFF":
            button.click()
            test.log("Enabling Auto Answer")
        elif button.text == "ON":
            test.log("Auto Answer is already enabled")

    def disable_auto_answer(self):
        """
        Function to disable AUto Answer
        :return:
        """
        button = driver.find_element_by_css_selector(self.auto_answer_button)
        if button.text == "ON":
            button.click()
            test.log("Disabling Auto Answer")
        elif button.text == "OFF":
            test.log("Auto Answer is already disabled")

    def enable_dnd(self):
        """
        Functin to Enable Do Not Disturb on VoIP dialer page
        :return:
        """
        if self.verify_dnd_status():
            test.log("DND is already enabled")
        else:
            driver.find_element_by_css_selector(self.dnd_disabled).click()
            test.log("Enabling DND")
            if self.verify_dnd_status():
                test.log("Successfully enabled DND")

    def disable_dnd(self):
        """
        Functin to disable Do Not Disturb on VoIP dialer page
        :return:
        """
        if self.verify_dnd_status() is False:
            test.log("DND is already disabled")
        else:
            driver.find_element_by_css_selector(self.dnd_enabled).click()
            test.log("Disabling DND")
            if self.verify_dnd_status() is False:
                test.log("Successfully disabled DND")

    def register_line(self, device, line=None, authentication=True, use_proxy=False):
        """
        Function to Register Line
        :param use_proxy:
        :param authentication:
        :param device: device
        :param line: line number
        :return:
        """
        device = device[:3] + device[-1]
        device = device.lower()

        if 0 < line <= 10:
            self.section = device + "_" + "line" + str(line)
        elif len(line) == 5:
            self.section = device + "_" + line.lower()
        elif len(line) == 1:
            self.section = device + "_" + "line" + line

        if use_proxy is False:
            self.set_register_parameters(username=config.get_data(self.section, "username"),
                                         auth_name=config.get_data(self.section, "auth_name"),
                                         password=config.get_data(self.section, "voip_password"),
                                         display_name=config.get_data(self.section, "display_name"),
                                         domain=config.get_data(self.section, "domain"),
                                         auth_enable=authentication)
            username = config.get_data(self.section, "username")
            domain = config.get_data(self.section, "domain")
        elif use_proxy is True:
            self.set_register_parameters(username=config.get_data(self.section, "username"),
                                         auth_name=config.get_data(self.section, "auth_name"),
                                         password=config.get_data(self.section, "voip_password"),
                                         display_name=config.get_data(self.section, "display_name"),
                                         proxy=config.get_data(self.section, "proxy"),
                                         auth_enable=authentication, use_domain=False)
            username = config.get_data(self.section, "username")
            domain = config.get_data(self.section, "proxy")

        self.reconfigure()
        self.wait("To get device registered", wait_seconds=2)
        test.verify(self.verify_registration_status() is True, "Verifying Line is registered")
        return username, domain

    def verify_dnd_status(self):
        """
        Function to verify DND status
        :return:
        """
        try:
            test.log("Checking DND State")
            driver.implicitly_wait(5)
            if driver.find_element_by_css_selector(self.dnd_enabled):
                test.log("DND is enabled")
                return True
        except NoSuchElementException:
            test.log("DND is disabled")
            return False

    def verify_redial_button_is_enabled(self):
        """
        Function to verify redial button is enabled
        :return: True or False
        """
        button = driver.find_element_by_css_selector(self.redial_button).text
        if button == "REDIAL":
            test.log("Found Redial button")
            return True
        else:
            test.log("Unable to find Redial button")
            return False

    def verify_hold_button_is_enabled(self):
        """
        Function to verify hold button is enabled
        :return:
        """
        if driver.find_element_by_css_selector(self.hold_button).is_enabled():
            test.log("Found Hold button")
            return True
        else:
            test.log("Unable to find Hold Key")
            return False

    def verify_resume_button_is_enabled(self):
        """
        Function to verify hold button is enabled
        :return:
        """
        if driver.find_element_by_css_selector(self.resume_button).is_enabled():
            test.log("Found Hold button")
            return True
        else:
            test.log("Unable to find Hold Key")
            return False

    def verify_mute_button_is_enabled(self):
        """
        Function to verify Mute is enabled
        :return:
        """
        if self.verify_call_state():
            button = driver.find_element_by_css_selector(self.mute).text
            if button == "MUTE":
                test.log("Mute button is available")
                return True
            else:
                test.log("Unable to find Mute button")
                return False
        else:
            raise AssertionError("Call is not Active, Mute button only appears in active call")

    def verify_unmute_button_is_enabled(self):
        """
        Function to verify Mute is enabled
        :return:
        """
        if self.verify_call_state():
            button = driver.find_element_by_css_selector(self.mute).text
            if button == "UNMUTE":
                test.log("UnMute button is available")
                return True
            else:
                test.log("Unable to find UnMute button")
                return False
        else:
            raise AssertionError("Call is not Active, Mute/UnMute button only appears in active call")

    def verify_accept_button_enabled(self):
        """
        Function to verify Accept button is displayed of incoming call on VoIP Dialer Window
        :return: True or False
        """
        if driver.find_element_by_css_selector(self._accept_button).is_displayed():
            test.log("Accept Button is enabled")
            return True
        else:
            test.log("Accept Button is not found")
            return False

    def verify_reject_button_enabled(self):
        """
        Function to verify Accept button is displayed of incoming call on VoIP Dialer Window
        :return: True or False
        """
        if driver.find_element_by_css_selector(self._reject_button).is_displayed():
            test.log("Reject Button is enabled")
            return True
        else:
            test.log("Reject Button is not found")
            return False

    def verify_call_state(self):
        """
        Function to verify call is active
        Note: Verifying active call by checking End Button is enabled or disabled
        :return:
        """
        if driver.find_element_by_css_selector(self._end_button).is_displayed():
            test.log("Call is Active")
            return True
        else:
            test.log("Call is not active")
            return False

    def verify_registration_status(self):
        """
        Function to check Account Status
        :return:
        """
        register_status = driver.find_element_by_xpath(self._account_status).text
        if register_status == "Registered (200)":
            test.log("Registered, Account Status is: {}".format(register_status))
            return True
        else:
            test.log("Not Registered, Account Status is: {}".format(register_status))
            return False
