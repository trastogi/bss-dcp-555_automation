source(findFile("scripts", "Utils/Base_Logging.py"))
# source(findFile("scripts", "Utils/get_config.py"))
source(findFile("scripts", "device/PageObject/DriverFactory.py"))
# import logging
import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


global config, driver

class PageObject:
    """Page class that all page models can inherit from"""
            
    def set_value_on_web(self, identifier, value, selector='xpath'):
        """
        Function to set value on web
        :param identifier: its the element that we want to find on web
        :param selector: its the selector e.g xpath, css_selector
        :param value: the value we want to set on web
        :return:
        """
        selector = selector.lower()
        if selector == "xpath":
            self.action = driver.find_element_by_xpath(identifier)
        elif selector == "css_selector":
            self.action = driver.find_element_by_css_selector(identifier)
        elif selector == "id":
            self.action = driver.find_element_by_id(identifier)
        elif selector == "class_name":
            self.action = driver.find_element_by_class_name(identifier)
        elif selector == "tag_name":
            self.action = driver.find_element_by_tag_name(identifier)
        elif selector == "link_text":
            self.action = driver.find_element_by_link_text(identifier)
        elif selector == "name":
            self.action = driver.find_element_by_name(identifier)
        elif selector == "partial_link_text":
            self.action = driver.find_element_by_partial_link_text(identifier)
        try:
            self.action.click()
            self.action.clear()
            self.action.send_keys(value)
        except Exception as error:
            self.log.write(error)
            raise AssertionError

    def wait(self, message, wait_seconds=5):
        """
        Function to give wait time with a message
        :param wait_seconds: time for wait in seconds
        :param message: reason for waiting
        :return:
        """
        test.log("Waiting for {} seconds, {}".format(wait_seconds, message))
        time.sleep(wait_seconds)

    def reconfigure(self):
        """
        Function to apply changes of device
        :return:
        """
        driver.find_element_by_css_selector(".settings-btn").click()
        log.write("Clicking on Reconfigure")
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, ".alert > strong:nth-child(3)")))
        message = driver.find_element_by_css_selector(".alert > strong:nth-child(3)").text
        test.verify(message == "Success!", "Verifying for Success Message")
#         if message == "Success!":
#             test.log("Successfully made changes to device")
#         else:
#             raise AssertionError("Unable to made changes")
    