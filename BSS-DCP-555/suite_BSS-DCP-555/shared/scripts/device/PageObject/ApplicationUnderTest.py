source(findFile("scripts", "/device/PageObject/LoginPage.py"))
source(findFile("scripts", "/device/PageObject/PageObject.py"))
# from LoginPage import LoginPage
# from PageObject import PageObject


# class Borg:
#     """
#     The borg design pattern is to share state
#     Src: http://code.activestate.com/recipes/66531/
#     """
#     __shared_state = {}
#  
#     def __init__(self):
#         self.__dict__ = self.__shared_state
#  
#     def is_first_time(self):
#         """
#         Has the child class been invoked before?
#         """
#         result_flag = False
#         if len(self.__dict__) == 0:
#             result_flag = True
#  
#         return result_flag
    
global config, driver

class BssDcp555(PageObject):
    """
    Device page class for common actions will perform on device web page
    """
#     def __init__(self):
#         """
#         Constructor for BssDCP555
#         :param base_url: web access method used to access device web page e.g. http or https
#         """
# #         Borg.__init__(self)
# #         if self.is_first_time():        
# #         self.config = GetConfig()
# #         self.driver_factory = DriverFactory()
# #         self.driver = self.driver_factory.get_web_driver(self.config.get_data("browsers", "browser"))
# #         self.driver.implicitly_wait(30)
# #         self.log = BaseLogging(level=logging.DEBUG)
#         PageObject.__init__(self)
# #         self.base_url = "https://"
#         self.config = GetConfig()
# #         self.driver = DriverFactory()
# #         self.driver = self.driver.get_web_driver(browser=self.config.get_data("browsers", "browser"))
# #         self.driver.implicitly_wait(30)
#         self.log = BaseLogging(level=logging.DEBUG)

    def open_web(self, url):
        """
        Open the device webpage
        :param url: device IP or FQDN
        :return:
        """
        webpage = "https://{}/#/login".format(config.get_data("device", str(url).lower()))
        driver.get(webpage)
        test.log("Opening Device WebPage")
        driver.maximize_window()

        return LoginPage()

    def close_web(self):
        """
        Closing the web browser
        :return:
        """
        test.log("Closing Device WebPage")
        driver.close()

    # def set_value_on_web(self, identifier, value, selector='xpath'):
    #     """
    #     Function to set value on web
    #     :param identifier: its the element that we want to find on web
    #     :param selector: its the selector e.g xpath, css_selector
    #     :param value: the value we want to set on web
    #     :return:
    #     """
    #     driver = self.driver
    #     selector = selector.lower()
    #     if selector == "xpath":
    #         self.action = driver.find_element_by_xpath(identifier)
    #     elif selector == "css_selector":
    #         self.action = driver.find_element_by_css_selector(identifier)
    #     elif selector == "id":
    #         self.action = driver.find_element_by_id(identifier)
    #     elif selector == "class_name":
    #         self.action = driver.find_element_by_class_name(identifier)
    #     elif selector == "tag_name":
    #         self.action = driver.find_element_by_tag_name(identifier)
    #     elif selector == "link_text":
    #         self.action = driver.find_element_by_link_text(identifier)
    #     elif selector == "name":
    #         self.action = driver.find_element_by_name(identifier)
    #     elif selector == "partial_link_text":
    #         self.action = driver.find_element_by_partial_link_text(identifier)
    #     try:
    #         self.action.click()
    #         self.action.clear()
    #         self.action.send_keys(value)
    #     except Exception as error:
    #         self.log.write(error)
    #         raise AssertionError

    # def wait(self, message, wait_seconds=5):
    #     """
    #     Function to give wait time with a message
    #     :param wait_seconds: time for wait in seconds
    #     :param message: reason for waiting
    #     :return:
    #     """
    #     print "Waiting for {} seconds, {}".format(wait_seconds, message)
    #     time.sleep(wait_seconds)

