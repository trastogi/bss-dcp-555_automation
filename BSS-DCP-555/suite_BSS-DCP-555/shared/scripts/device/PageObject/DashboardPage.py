"""
This Page Object module contains the Main Page of the device i.e Dashboard
and if the Configuration Card is selected then Audio Setting Page will be the main page of the device
"""
source(findFile("scripts", "/device/PageObject/VoipSettingsPage.py"))
source(findFile("scripts", "/device/PageObject/PageObject.py"))
# from PageObject import PageObject
# from BasePage import BasePage
# from VoipSettingsPage import VoipSettings
# from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

global config, driver, log


class Dashboard(PageObject):
    """
    Page Object class for main page of the device
    """

    def __init__(self):
        PageObject.__init__(self)

        form = driver.find_element_by_css_selector(".heading").text
        test.verify(form == "Dashboard", "Verifying we are in Dashboard Page")

#         if form == "Dashboard":
#             log.write("We are in Dashboard Page")
#         else:
#             raise AssertionError("We are into Wrong page")

        self.create_new = "button.btn-info:nth-child(2)"
        self.card_name = ".align-center > h4:nth-child(1)"
        self.other_card_name = ".italics"
        self.popup_warn = ".vertical-align-center > div:nth-child(1) > div:nth-child(3) > button:nth-child(2)"
        self.input_name = "#configNewName"
        self.input_name_ok = "#create > div:nth-child(1) > div:nth-child(1) > div:nth-child(3) > button:nth-child(2)"
        self.disconnected = "#disconnected-title"
        self.delete_card = "//*[@tooltip='Delete']"
        self.password_fild = "//input[@type='password']"

    def create_card(self, card_name="Test"):
        """
        Function to create a configuration card on the device
        :param card_name: Name the configuration
        :return:
        """
        try:
            if driver.find_element_by_css_selector(self.active_card):
                log.write("Already one active configuration file is present, creating a new one")
                if driver.find_element_by_css_selector(self.card_name).text == card_name or \
                        driver.find_element_by_css_selector(self.other_card_name).text == card_name:
                    card = driver.find_element_by_css_selector(self.other_card_name).text
                    log.write("Already one card with same name, creating a new one: {}".format(card + "1"))
                    self.create_config_card(card + "1")
                    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR,
                                                                                         self.disconnected)))
                    WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.XPATH,
                                                                                         self.password_fild)))
                    self.set_password(config.get_data("device", "device_password"))
                    self.sign_in()
            else:
                test.log("No active configuration file found on device, creating a new one")
                self.create_config_card(card_name)
        except Exception as e:
            test.log(e, level="warning")

    def create_config_card(self, card_name):
        """
        Function to create configuration
        :param card_name:
        :return:
        """
        driver.find_element_by_css_selector(self.create_new).click()
        if WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR,self.popup_warn))):
            driver.find_element_by_css_selector(self.popup_warn).click()
        else:
            pass
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR,
                                                                             self.input_name)))
        self.set_value_on_web(self.input_name, card_name, selector="css_selector")
        driver.find_element_by_css_selector(self.input_name_ok).click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR,
                                                                             self.sucess_msg)))
        message = driver.find_element_by_css_selector(self.sucess_msg).text
        if message == "Created":
            test.log("Successfully created Configuration Card")
        else:
            test.log(message, level="error")
            raise AssertionError("Unable to create new configuration")

    def delete_conf_card(self):
        """
        Function to delete configuration card
        :return:
        """
        pass
