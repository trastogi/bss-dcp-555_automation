source(findFile("scripts", "Utils/get_config.py"))
import os
import platform

from selenium import webdriver


class DriverFactory:
    """
    DriverFactory class,
    Support for choosing browsers for test
    """
    def __init__(self):
        self.os_name = self._get_os_name()

    def _get_os_name(self):
        """ Returns the current OS name (platform) e.g. windows"""
        current_os = platform.system()
        return current_os

    def _get_driver_path(self, driver):
        """
        Function to get the browser driver path
        :param driver: driver name e.g if browser is firefox, then driver is geckodriver
        :return: Path for the driver as a string
        """
        driver_path = os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'shared/drivers')), '{}.exe'.format(driver))
        return str(driver_path)

    def get_web_driver(self, browser):
        """
        Function to return browser driver
        :param browser:
        :return: browser driver
        """
        local_driver = None
        os_name = self.os_name

        if os_name.lower() == 'windows':
            if str(browser).lower() == 'firefox' or str(browser).lower() == 'ff':
                print self._get_driver_path('geckodriver')
                local_driver = webdriver.Firefox(executable_path=self._get_driver_path('geckodriver'))
            elif str(browser).lower() == 'ie':
                local_driver = webdriver.Ie(executable_path=self._get_driver_path('IEDriverServer'))
            elif str(browser).lower() == 'chrome':
                local_driver = webdriver.Chrome(executable_path=self._get_driver_path('chromedriver'))
            elif str(browser).lower() == 'safari':
                local_driver = webdriver.Safari()
        elif os_name.lower() == 'linux':
            if str(browser).lower() == 'firefox' or browser.lower() == 'ff':
                local_driver = webdriver.Firefox()
            elif str(browser).lower() == 'chrome':
                local_driver = webdriver.Chrome()
            elif str(browser).lower() == 'ie':
                test.log("This is Linux system, and it currently doesn't have IE Browser, So trying to run the " \
                          "application on FireFox")
                local_driver = webdriver.Firefox()

        return local_driver
