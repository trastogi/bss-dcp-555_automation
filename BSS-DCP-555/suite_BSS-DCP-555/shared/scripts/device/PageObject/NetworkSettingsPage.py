"""
This Page Object module contains the page classes for different configurations
can be done on the device
"""
source(findFile("scripts", "/device/PageObject/PageObject.py"))
# from PageObject import PageObject

global config, driver, log


class NetworkSettings(PageObject):
    """
    Page Object class for Network Settings page
    """

    def __init__(self):
#         PageObject.__init__(self)

        page_name = driver.find_element_by_css_selector("div.col-md-4:nth-child(1)").text
        test.verify(page_name == "Network Settings", "Verifying we are in Network Settings page")
#         if page_name == "Network Settings":
#             test.log("Network Settings Page opened")
#         else:
#             raise AssertionError("Unable to open Network Settings page")

        # CSS_Selectors for the elements on page
        self.portA = "li.active:nth-child(1) > a:nth-child(1)"
        self.portB = ""

    def navigate_to_port_a(self):
        """
        Function to navigate to Port A (AES67)
        :return:
        """
        pass

    def navigate_to_port_b(self):
        """
        Function to navigate to Port B (VoIP)
        :return:
        """
        pass

