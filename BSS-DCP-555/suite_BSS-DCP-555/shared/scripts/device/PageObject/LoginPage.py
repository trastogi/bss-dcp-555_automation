# source(findFile("scripts", "/device/PageObject/ApplicationUnderTest.py"))
source(findFile("scripts", "/device/PageObject/BasePage.py"))
source(findFile("scripts", "/device/PageObject/PageObject.py"))

# from device.PageObject.ApplicationUnderTest import BssDcp555
# from device.PageObject.PageObject import PageObject
# from device.PageObject.BasePage import BasePage

global config, driver, log

class LoginPage(PageObject):
    """
    Page Object for the login page
    """

    def __init__(self):

        #PageObject.__init__(self)
        FORM = "//*[@id='welcome-header']"
        
        test.verify(driver.find_element_by_xpath(FORM).text == "BSS DCP-555 Configuration", "Verifying We are in Login Page")
        #self.driver = driver
#         if driver.find_element_by_xpath(FORM).text == "BSS DCP-555 Configuration":
#             log.write("Login Page Opened")
#         else:
#             raise AssertionError("Unable to open Login Page")
        # Xpath for all fields
        self.password_fild = "//input[@type='password']"
        self.signin_button = "//button[@type='submit']"
        self.logout = "//*[@id='logout']"

    def login(self, password=""):
        """
        Login to device page
        :param url: url for device, IP or FQDN
        :param password: device password
        :return: True or False
        """
        # Open web page of the device
        # self.open_web(url)
        # assert self.driver.title() == "BSS DCP-555 Configuration"

        self._set_password(config.get_data("device", password.lower()))
        self._sign_in()
        return BasePage()

    def _set_password(self, password):
        """
        Set the password on the login screen
        :param password: device password
        :return:
        """
        self.set_value_on_web(self.password_fild, password)

    def _sign_in(self):
        """
        Click on Sign In button on login page
        :return:
        """
        driver.find_element_by_xpath(self.signin_button).click()
