source(findFile("scripts", "/device/PageObject/PageObject.py"))
source(findFile("scripts", "/device/PageObject/DashboardPage.py"))
source(findFile("scripts", "/device/PageObject/NetworkSettingsPage.py"))
source(findFile("scripts", "/device/PageObject/AudioSettingsPage.py"))
source(findFile("scripts", "/device/PageObject/VoipSettingsPage.py"))
# from PageObject import PageObject
# from DashboardPage import Dashboard
# from NetworkSettingsPage import NetworkSettings
# from VoipSettingsPage import VoipSettings
# from AudioSettingsPage import AudioSettings
# source(findFile("scripts", "/config/config.ini"))

global config, driver, log

 
class BasePage(PageObject):
    """
    Page Object for Sidebar and Top Bar
    """

    def __init__(self):

        FORM = ".heading"
        test.verify(driver.find_element_by_css_selector(FORM).text == "Dashboard", "Verifying we are in Main Page")

#         if driver.find_element_by_css_selector(FORM).text == "Dashboard":
#             log.write("Login Successful, Now in Base Page")
#         else:
#             raise AssertionError("Login Failure !!!")

        # XPATH
        self.network_settings = "//*[@href='#/settings/networksettings']"
        self.device_settings = "//*[@href='#/settings/devicesettings']"
        self.voip_settings = "//*[@href='#/settings/voipsettings']"
        # CSS_SELECTOR for navigation menu
        self.dashboard = ".logo-mini"
        self.settings = "li.treeview:nth-child(2)"
        self.third_party_control = "li.treeview:nth-child(3) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1) > " \
                                   "span:nth-child(2)"
        self.audio_settings = ".active > a:nth-child(1) > span:nth-child(2)"
        self.blu_link_settings = "li.treeview:nth-child(2) > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1) > " \
                                 "span:nth-child(2)"
        self.gpio_settings = "li.treeview:nth-child(2) > ul:nth-child(2) > li:nth-child(5) > a:nth-child(1) > " \
                             "span:nth-child(2)"
        self.rs232_settings = "li.treeview:nth-child(2) > ul:nth-child(2) > li:nth-child(6) > a:nth-child(1) > " \
                              "span:nth-child(2)"
        self.active_card = ".card"

    def get_setting_page(self, setting):
        """
        Function to navigate around settings page
        :param setting:
        :return:
        """
        setting = setting.lower()
        if setting == 'dashboard':
            driver.find_element_by_css_selector(self.dashboard).click()
            return Dashboard()
        elif setting == 'voip_settings' or 'voipsettings' or 'voip settings' or 'voip':
            self.select_config_card()
            driver.find_element_by_css_selector(self.settings).click()
            driver.find_element_by_xpath(self.voip_settings).click()
            return VoipSettings()
        elif setting == 'network_settings' or 'networksettings' or 'network settings' or 'network':
            driver.find_element_by_css_selector(self.settings).click()
            driver.find_element_by_xpath(self.network_settings).click()
            return NetworkSettings()
        elif setting == 'device_settings' or 'devicesettings' or 'device settings' or 'device':
            driver.find_element_by_css_selector(self.settings).click()
            driver.find_element_by_xpath(self.device_settings).click()
            return True
        elif setting == 'audio_settings' or 'audiosettings' or 'audio settings' or 'audio':
            self.select_config_card()
            driver.find_element_by_css_selector(self.settings).click()
            driver.find_element_by_css_selector(self.audio_settings).click()
            return AudioSettings()

    # def navigate_to_dashboard(self):
    #     """
    #     Function to navigate to Dashboard
    #     :return: True or False
    #     """
    #     # self.driver.find_element_by_css_selector("li.treeview:nth-child(1)").click()
    #     self.driver.find_element_by_css_selector(self.dashboard).click()
    #     dashboard = self.driver.find_element_by_css_selector(".heading").text
    #     if dashboard == "Dashboard":
    #         self.log.write("Successfully Navigated to Dashboard")
    #         return True
    #     else:
    #         self.log.write("Navigation to Dashboard Failed")
    #         return False

    def select_config_card(self):
        """
        Function to select the active configuration card
        :return:
        """
        if driver.find_element_by_css_selector(self.active_card):
            test.log("Found one Active Configuration Card")
            test.log("Clicking on Active Configuration Card")
            driver.find_element_by_css_selector(self.active_card).click()
        else:
            raise AssertionError("Unable to find config card")


