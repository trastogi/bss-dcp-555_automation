# -*- coding: utf-8 -*-
source(findFile("scripts", "Utils/get_config.py"))
source(findFile("scripts", "device/PageObject/DriverFactory.py"))
source(findFile("scripts", "Utils/Base_logging.py"))
import os

config = None
driver = None
log = None
file_name = None

def init():
    
    global config, driver, log, file_name
    
    config = GetConfig()
    log = BaseLogging()
    file_name = os.path.dirname(__file__)
    
    driver_factory = DriverFactory()
    driver = driver_factory.get_web_driver(config.get_data("browsers", "browser"))
    
    