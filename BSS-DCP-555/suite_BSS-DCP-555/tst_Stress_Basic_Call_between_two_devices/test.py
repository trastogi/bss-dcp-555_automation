"""
#$Id: Outgoing dialing number

Name:
     Outgoing dialing number

Test Case Coverage:
     This script will cover below test cases:
     1. Sec1_1 : Outgoing dialing number
     2. Sec1_2 : Call Dial Button
     3. Sec1_3 : Call End Button
     4. Sec1_4 : Accept Button


Author:
     Awadhesh Kumar (awadhesh.kumar@harman.com)

Purpose:
     To test DUT successfully establish a call and gracefully ends the call

Description:
      From Line 1 DUT dials the number for Line 2, and receives the call at line 2.
      After successfully establishing the call, DUT ends the call from Line 2.

Test bed requirement:
      One DCP-555 device

Test Steps:
    1. From Line 1, Launch the VoIP dialer, and dial Line 2 number
    2. Navigate to line 2, Answer the call
    3. End the call from line 2

Verify:
      - DUT successfully establishes the call session.
      - SIP Flow:
        Line 1                   Server
          | ----- INVITE --------> |
          | <----- 401 ----------- |
          | ------- ACK ---------> |
          | ----- INVITE --------> |
          | <----- 180 ----------- |
          | <----- 200 ----------- |
          | ------- ACK ---------> |
          ========= RTP ============
          | <----- BYE ----------- |
          | ------- 200 ---------> |

End of Header
#################################################################
"""

source(findFile("scripts", "/device/PageObject/ApplicationUnderTest.py"))
source(findFile("scripts", "suite_init.py"))
source(findFile("scripts", "Utils/tshark.py"))


def main():
    
    num_of_itr = 2
    # Device 1 
    device1 = BssDcp555()
    dev1_tab = driver.window_handles[0]
    dev1_login_page = device1.open_web(url="device1")
    dev1_base_page = dev1_login_page.login(password="device1_password")
    dev1_voip_settings = dev1_base_page.get_setting_page("VoIP")
    dev1_voip_settings.navigate_to_line("line1")
    dev1_voip_settings.wait("....", wait_seconds=2)
    dev1_voip_settings.register_line("device1", "line1")
    device1.wait("....", wait_seconds=2)
    
    nativeType("<Ctrl+t>")
    device1.wait("....", wait_seconds=2)
    dev2_tab = driver.window_handles[1]
    driver.switch_to_window(dev2_tab)
    device2 = BssDcp555()
    dev2_login_page = device2.open_web(url="device2")
    dev2_base_page = dev2_login_page.login(password="device2_password")
    dev2_voip_settings = dev2_base_page.get_setting_page("VoIP")
    dev2_voip_settings.navigate_to_line("line1")
    user2, server = dev2_voip_settings.register_line("device2", "Line1")
    dev1_voip_settings.wait("....", wait_seconds=2)
      
    for i in range(num_of_itr):
        test.log("Running iteration No: {}".format(i + 1))
        
        driver.switch_to_window(dev1_tab)
        dev1_voip_settings.wait("To navigate to tab", wait_seconds=2)
        dev1_voip_settings.launch_dialer()
        dev1_voip_settings.wait("Waiting for VoIP Dialer Page", wait_seconds=2)
        dev1_voip_settings.dial(user2)
        dev1_voip_settings.press_call_button()
    #     assert voip_settings.verify_call_state() is True
        test.verify(dev1_voip_settings.verify_call_state() == True, "verifying if the call")
        
        driver.switch_to_window(dev2_tab)
        dev2_voip_settings.wait("To navigate to tab", wait_seconds=2)
        dev2_voip_settings.launch_dialer()
        dev2_voip_settings.wait("Waiting for VoIP Dialer Page", wait_seconds=2)
        test.verify(dev2_voip_settings.verify_accept_button_enabled()is True, "Verifying Accept Button is Enabled")
        dev2_voip_settings.accept_call()
    
        dev2_voip_settings.wait("Just waiting", wait_seconds=5)
        test.verify(dev2_voip_settings.verify_call_state() == True, "verifying Call is active")
        # Ending call from line 2
        dev2_voip_settings.press_end_call_button()
        dev2_voip_settings.wait("Waiting for call get disconnected", wait_seconds=2)
        test.verify(dev2_voip_settings.verify_call_state() is False, "verifying Call is inactive")
   
    dev2_voip_settings.close_dialer()
    nativeType("<Ctrl+w>")
    
    driver.switch_to_window(dev1_tab) 
    dev1_voip_settings.wait("To navigate to tab", wait_seconds=2)
    dev1_voip_settings.close_dialer()
    device1.close_web()

    
