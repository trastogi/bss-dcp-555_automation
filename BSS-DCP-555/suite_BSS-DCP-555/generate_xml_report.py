import json
import xml.etree.ElementTree as ET
import time
import xmltodict
import os
import sys
import shutil


project_name = "BSS DCP-555"
release_name = sys.argv[3]
testcycle_name = sys.argv[4]

filename = sys.argv[1]
local_squish_report_path = os.path.abspath(os.path.dirname(filename))
local_dashboard_report_path = sys.argv[2]
dashboard_server_path = "\\\\hiblw7abs01\\TestReports\\{}\\{}\\{}".format(project_name, release_name, testcycle_name)


def generate_dashboard_xml_report(squish_xml_report):

    with open(squish_xml_report, 'r') as f:
        xml_str = xmltodict.parse(f)

    with open("{}\\squish_result.json".format(local_squish_report_path), 'w+') as f:
        f.write(json.dumps(xml_str, indent=4))

    with open("{}\\squish_result.json".format(local_squish_report_path), 'r') as f:
        json_report = json.load(f)

    total_tc = json_report['SquishReport']['summary']['@testcases']

    pass_count = 0
    fail_count = 0
    norun_count = 0

    data = json_report['SquishReport']['testresult']

    for i in range(len(data)):
        if data[i]['@result'] == 'START_TEST_CASE':
            if int(data[i]['@fatals']) != 0:
                norun_count += 1
            else:
                if (int(data[i]['@fails']) == 0) and (int(data[i]['@errors']) == 0):
                    pass_count += 1
                else:
                    fail_count += 1
        if data[i]['@message'] == "Start 'suite_BSS-DCP-555'":
            suite_start_time = data[i]['@time']
        if data[i]['@message'] == "End 'suite_BSS-DCP-555'":
            suite_end_time = data[i]['@time']

    start_time = time.strftime("%b %d, %Y, %I:%M:%S %p", time.strptime(suite_start_time, "%Y-%m-%dT%H:%M:%S"))
    end_time = time.strftime("%b %d, %Y, %I:%M:%S %p", time.strptime(suite_end_time, "%Y-%m-%dT%H:%M:%S"))
    t1 = time.mktime(time.strptime(suite_start_time, "%Y-%m-%dT%H:%M:%S"))
    t2 = time.mktime(time.strptime(suite_end_time, "%Y-%m-%dT%H:%M:%S"))
    duration = time.strftime("%H:%M:%S", time.gmtime(t2 - t1))
    folder_name = time.strftime("%Y-%m-%dT%H-%M-%S", time.strptime(suite_start_time, "%Y-%m-%dT%H:%M:%S"))

    dataset = {
        'total_tc': total_tc,
        'start_time': start_time,
        'end_time': end_time,
        'duration': duration,
        'pass_count': str(pass_count),
        'fail_count': str(fail_count),
        'norun_count': str(norun_count)}

    xml_writer(dataset)

    return folder_name


def xml_writer(data):

    root = ET.Element("testreport")

    ET.SubElement(root, "project").text = project_name
    ET.SubElement(root, "testrelease").text = release_name
    ET.SubElement(root, "testcycle").text = testcycle_name
    doc = ET.SubElement(root, "report")
    ET.SubElement(doc, "duration").text = data['duration']
    ET.SubElement(doc, "starttime").text = data['start_time']
    ET.SubElement(doc, "endtime").text = data['end_time']
    ET.SubElement(doc, "scriptscount").text = data['total_tc']
    ET.SubElement(doc, "passcount").text = data['pass_count']
    ET.SubElement(doc, "failcount").text = data['fail_count']
    ET.SubElement(doc, "noruncount").text = data['norun_count']

    tree = ET.ElementTree(root)
    tree.write("{}\\report.xml".format(local_dashboard_report_path))


def main():
    ''' Generating dashbpard generic xml template '''
    folder_name = generate_dashboard_xml_report(filename)

    '''Copying Squish generated html report to local Dashboard report folder'''
    squish_html_report_path = os.path.join(local_squish_report_path, "Result")

    try:
        os.makedirs(local_dashboard_report_path)
    except Exception:
        pass

    dashboard_html_report_path = os.path.join(local_dashboard_report_path, "Result")
    shutil.copytree(squish_html_report_path, dashboard_html_report_path)

    '''Copying from local dashboard report to dashboard server'''
    dashboard_report_path = os.path.join(dashboard_server_path, folder_name)
    shutil.copytree(local_dashboard_report_path, dashboard_report_path)


if __name__ == "__main__":
    main()
