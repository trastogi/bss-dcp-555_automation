Pre-requisites:

1. Install Squish for Web
2. Setup Squish bin path to Environmental variables under system variables->Path (e.g. C:\Users\<user>\Squish for Web 6.4.0beta\bin)
3. Under Squish-python, install the following packages:
	a. pyshark
	b. paramiko
	c. selenium
4. Install Wireshark on your system


Instrucutions:
1. Copy the main folder (BSS-DCP-555) with suite_BSS-DCP-555 folder onto desktop.
2. Select the correct job from Jenkins (created for windows) and execute.


Important Point to Note:
Job execution from Jenkins will always create a DASHBOARD report.
Hence, for purposes like debugging/script enhancement/trial runs, please run the suite directly from Squish and NOT from Jenkins.